import System from "systemjs";
import "../config";
import should from "should";
import _ from "lodash";
import { TestWithDisabledConsoleLogging } from "./test-util";
import Q from "q";

const TEST_TINDER_TOKEN = "1234abcdefghij";
const TEST_URL = "http://nowhere.com/api";
const POST_TEST_PARAMS = {properties: 'present'};
const AUTH_ERROR_RESPONSE = {
  status: 401
};

// Mock SystemJS fetch import (mocked fetch will simply return the URL and the options)
const MOCK_FETCH_MODULE = {
  fetch: (url, opts) => Promise.resolve({url, opts})
};
System.set("vendor/fetch", System.newModule(MOCK_FETCH_MODULE));

describe("Tinder API", () => {

  it("performs a fetch-based POST as expected", done => {
    System.import("./js/tinder-api")
      .then(api => {
        let tinderAPI = api.default;

        // Perform POST with API, manually verify url and opts that are returned
        tinderAPI.POST(TEST_URL, POST_TEST_PARAMS)
          .then(res => {
            res.url.should.be.exactly(TEST_URL);
            res.opts.should.have.keys("body", "headers", "method");
            res.opts.method.should.be.exactly("post");
            res.opts.body.should.be.exactly(JSON.stringify(POST_TEST_PARAMS));
            done();
          })
          .catch(done);
      })
      .catch(done);
  });

  it("includes a tinder token when a POST with token is made", done => {
    System.import("./js/tinder-api")
      .then(api => {
        let tinderAPI = api.default;

        // Perform POST with API, manually verify url and opts that are returned
        tinderAPI.POSTWithToken(TEST_URL, POST_TEST_PARAMS, TEST_TINDER_TOKEN)
          .then((res) => {
            res.url.should.be.exactly(TEST_URL);
            res.opts.should.have.keys("body", "headers", "method");
            res.opts.body.should.be.exactly(JSON.stringify(POST_TEST_PARAMS));
            res.opts.method.should.be.exactly("post");

            // Expect token to be present and equal to mock value
            should.exist(res.opts.headers['X-Auth-Token']);
            res.opts.headers['X-Auth-Token'].should.be.exactly(TEST_TINDER_TOKEN);
            done();
          })
          .catch(done);
      })
      .catch(done);
  });

  it("includes a tinder token when a GET with token is made", done => {
    System.import("./js/tinder-api")
      .then(api => {
        let tinderAPI = api.default;

        // Perform GET with API, manually verify url and opts that are returned
        tinderAPI.GETWithToken(TEST_URL, TEST_TINDER_TOKEN)
          .then((res) => {
            res.url.should.be.exactly(TEST_URL);
            res.opts.should.have.keys("headers");
            res.opts.should.not.have.keys("method"); // since GET is default method

            // Expect token to be present and equal to mock value
            should.exist(res.opts.headers['X-Auth-Token']);
            res.opts.headers['X-Auth-Token'].should.be.exactly(TEST_TINDER_TOKEN);
            done();
          })
          .catch(done);
      })
      .catch(done);
  });

  it("detects authentication error responses properly", TestWithDisabledConsoleLogging(done => {
    System.import("./js/tinder-api")
      .then(api => {
        let tinderAPI = api.default;

        // Authentication retries should increase
        let resp = {data: "Working response"};
        let noErrorResponse = tinderAPI.detectAuthErrorResponse(resp);
        let errorResponse = tinderAPI.detectAuthErrorResponse(AUTH_ERROR_RESPONSE);

        noErrorResponse.should.eql(resp);
        should.not.exist(errorResponse);
        done();
      })
      .catch(done);
  }));

});

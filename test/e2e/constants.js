const SERVER_PORT = 8080;
const BASE_URL = "http://localhost";
const DEFAULT_TIMEOUT = 10000;

// Note, the default phantom config allows for caching,
// so some code changes will not be seen if caching rules are bad on local test http server
const DEFAULT_NM_CONFIG = {
  switches: {
    'disable-http-cache': true,
    'no-proxy-server': true
  }
};

function BASE_URL_WITH_PORT(port) {
  return `${BASE_URL}:${port}`;
}

export default {
  SERVER_PORT,
  BASE_URL,
  DEFAULT_TIMEOUT,
  DEFAULT_NM_CONFIG,
  BASE_URL_WITH_PORT
};

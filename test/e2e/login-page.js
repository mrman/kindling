import Q from 'q';
import Static from 'node-static';
import Http from 'http';
import NM from 'nightmare';
import "should";
import _ from "lodash";
import E2ETestConstants from "./constants.js";
import { clearLocalAndSessionStorage } from "./utils";

// Test constants
let STATIC_SERVER;
let NODE_SERVER;
const DIST_PATH = `${__dirname}/../../dist`;
const TEST_SERVER_PORT = E2ETestConstants.SERVER_PORT;

// Page constants
const LOGIN_BUTTON_ID = "#login-button";

describe("Login page", function() {
  this.timeout(E2ETestConstants.DEFAULT_TIMEOUT);
  let nm;

  before(done => {
    // Init nightmare instance (and electron process)
    nm = NM(E2ETestConstants.DEFAULT_NM_CONFIG);

    // Create static server
    STATIC_SERVER = new Static.Server(DIST_PATH);
    NODE_SERVER = Http.createServer((req, resp) => {
      req.addListener('end', () => {
        STATIC_SERVER.serve(req, resp);
      }).resume();
    }).listen(E2ETestConstants.SERVER_PORT, () => {

      let testServerUrl = E2ETestConstants.BASE_URL_WITH_PORT(TEST_SERVER_PORT);

      // Setup test suite
      nm.goto(testServerUrl)
        .then(clearLocalAndSessionStorage(nm))
        .then(done);

    });
  });

  it("Should have the appropriate title", (done) => {
    nm.title().then(title => {
      title.should.match(/Kindling/);
      done();
    });
  });

  it("Should show the login button for a user that isn't logged in", done => {
    nm.exists(LOGIN_BUTTON_ID).then(exists => {
      nm.visible(LOGIN_BUTTON_ID).then(visible => {
        (exists && visible).should.be.true();
        done();
      });
    });
  });

  after(done => {
    // End the electron process and the node server
    Promise.resolve(nm.end()).then(() => {
      NODE_SERVER.close(done);
    });
  });


});

/**
 * Returns a promise that when executed, performs some function on a nightmare instance and returns the result.
*
 * @param {Object} nm - The nightmare instance on which to perform the function
 * @param {Function} fn - The function to perform, which will be given the nightmare instance
 */
function doNMAction(nm, fn) {
  return Q.Promise((resolve, reject) => {
    fn(nm)
      .then(resolve)
      .catch(reject);
  });
}

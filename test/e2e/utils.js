import Q from 'q';
import NM from 'nightmare';
import E2ETestConstants from './constants';

const DEFAULT_TOKEN_INFO = {
  accessToken: "TEST_USER_ACCESS_TOKEN",
  expiresIn: "9999999"
};

/**
 * Generate the URL that would be redirected to as a result of successful/failing auth
 *
 * @param {string} url - The base URL
 * @param {Object} tokenInfo - The information that should have been passed along with the token
 * @param {string} tokenInfo.accessToken - The access token
 * @param {string} tokenInfo.expiresIn - A stringified number representing when the token expires
 */
function generateMockLoginResponseUrl(url, tokenInfo=DEFAULT_TOKEN_INFO) {
  return url + `/index.html#/access_token=${tokenInfo.accessToken}&expires_in=${tokenInfo.expiresIn}`;
}

/**
 * Login test user and go to main app page
 */
NM.action('fakeLoginResponseAtURL', function(url, done) {
  this.goto(generateMockLoginResponseUrl(url));
  done();
});

/**
 * Clear local and session storage, especially for test setup
 */
NM.action('clearLocalAndSessionStorage', function(done) {
  Q.Promise.resolve(
    this.evaluate(() => {
      localStorage.clear();
      sessionStorage.clear();
    })
  ).then(done);
});

/**
 * Force Nightmare to navigate to a given URL as a result of authentication.
 * A URL like "localhost:8080/" would have a fake redirect like "localhost:8080/#/index.html#access_token=..."
 *
 * @param {Object} nm - the nightmare instance to use
 * @param {string} url - the URL that will be used as the base for the fake redirect URL
 * @returns A function that when executed, returns a promise that causes NM to navigate
 */
export function doFakeLoginResponseAtURL(nm, url) {
  return () => {
    return Q.Promise((resolve, reject) => {
      nm.goto(generateMockLoginResponseUrl(url));
      resolve();
    });
  };
};

/**
 * Clear local and session storage when called
 *
 * @param {Object} nm - The nightmare instance to use
 * @return A Promise that resolves when the storages have been cleared
 */
export function clearLocalAndSessionStorage(nm) {
  return () => {
    return Q.Promise((resolve, reject) => {
      let res = nm.evaluate(() => {
        localStorage.clear();
        sessionStorage.clear();
      });
      resolve();
    });
  };
};

/**
 * Mock a fetch call
 */
export function mockFetch(nm, urlRegex, response) {
  return nm.evaluate(function() {
    window.originalFetch = window.fetch;
    window.fetch = function(url, opts) {

      if (url.match(urlRegex) != null) {
        return response;
      } else {
        return originalFetch(url, opts);
      }

    };
  });
};

import Q from 'q';
import Static from 'node-static';
import Http from 'http';
import NM from 'nightmare';
import "should";
import { every } from "lodash";
import { clearLocalAndSessionStorage, doFakeLoginResponseAtURL, mockFetch } from "./utils";
import E2ETestConstants from "./constants";

// Test constants
let STATIC_SERVER;
let NODE_SERVER;
const DIST_PATH = `${__dirname}/../../dist`;
const TEST_SERVER_PORT = E2ETestConstants.SERVER_PORT + 1; // Hack to ensure that tests  don't collide

describe("App main page", function() {
  this.timeout(E2ETestConstants.DEFAULT_TIMEOUT);
  let nm;

  before(done => {
    // Init nightmare instance (and electron process)
    nm = NM(E2ETestConstants.DEFAULT_NM_CONFIG);

    // Create static server
    STATIC_SERVER = new Static.Server(DIST_PATH);
    NODE_SERVER = Http.createServer((req, resp) => {
      req.addListener('end', () => {
        STATIC_SERVER.serve(req, resp);
      }).resume();
    }).listen(TEST_SERVER_PORT, () => {

      let testServerUrl = E2ETestConstants.BASE_URL_WITH_PORT(TEST_SERVER_PORT);

      // Setup test suite
      nm.goto(testServerUrl)
        .then(clearLocalAndSessionStorage(nm))
        .then(doFakeLoginResponseAtURL(nm, testServerUrl))
        .then(done);

      // mockFetch(nm, /https:\/\/api.gotinder.com\/auth/, {data: "some data"})
      
    });
  });

  it("The app main page should be up", (done) => {
    nm.url()
      .then(url => {
        url.should.match(/index.html#\/app/);
        done();
      });
  });

  it("should contain expected navigation links", (done) => {
    nm.exists("#recs-page-link-button").then(first => {
      nm.exists("#preferences-page-link-button").then(second => {
        nm.exists("#matches-page-link-button").then(third => {
          every([first,second, third]).should.be.true();
          done();
        });
      });
    });
  });
    
  after(done => {
    // End the electron process and the node server
    Promise.resolve(nm.end()).then(() => {
      NODE_SERVER.close(done);
    });
  });

});

/**
 * Wrap an nightmare exists call in a proper promise
 *
 * @param {object} nm - Nightmare instance
 * @param {string} selector - The selector to look for
 * @returns A Promise that resolves to true or rejects (if exists returns false)
*/
function existsPromise(nm, selector) {
  return Q.Promise((resolve, reject) => {
    nm.exists(selector)
      .then(res => {
        if (res) {
          resolve(true);
        } else {
          reject(`Failed to find selector ${selector}`);
        }
      });
  });
}

/**
 * Modify an async function to disable before/after it runs
 *
 * @param {function} done - The function that is called when done
 * @param {function} fn - The steps to be performed, this function will be passed a modified done()
 * @returns {function} fn - The steps to be performed, this function will be passed a modified done()
 */
export function TestWithDisabledConsoleLogging(fn) {
  return (done) => {
    // Backup and disable console.log
    let OLD_CONSOLE_LOG = console.log;
    console.log = undefined;

    // Augment done function
    let newDone = () => {
      console.log = OLD_CONSOLE_LOG;
      done();
    };

    // Run the actual function with the modified done callback
    fn(newDone);
  };
}

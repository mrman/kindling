import System from "systemjs";
import "../config";
import _ from "lodash";
import should from "should";
import sinon from "sinon";
import "should-sinon";

const TEST_URL = "http://nowhere.com/api";
const POST_TEST_PARAMS = {properties: 'present'};

const FAILURE_LOC_HASH = "error=bad&error_code=400&error_description=test error&error_reason=testing";
const SUCCESS_LOC_HASH = "access_token=1234abcdefg&expires_in=146000";
const FAILURE_AND_SUCCESS_LOC_HASH = `${FAILURE_LOC_HASH}&${SUCCESS_LOC_HASH}`;

const INVALID_TINDER_USER_PREFS = {
  INCOMPLETE: {
    distance_filter: 10,
    age_filter_min: 21
  },
  INVALID_MIN: {
    distance_filter: 10,
    age_filter_min: "twenty five",
    age_filter_max: 25,
    gender: 0
  }
};
  
const VALID_TINDER_USER_PREF_OBJ = {
  distance_filter: 50,
  age_filter_min: 25,
  age_filter_max: 40,
  gender: 0
};

// Mock SystemJS fetch import (mocked fetch will simply return the URL and the options)
const MOCK_FETCH_MODULE = {
  fetch: (url, opts) => Promise.resolve({url, opts})
};
System.set("vendor/fetch", System.newModule({default: MOCK_FETCH_MODULE}));

describe("Utility functions", () => {

  it("Should generate a URL with query parameters properly", (done) => {
    System.import("./js/util")
      .then(util => {
        let generatedUrl = util.generateURLWithQueryParams(TEST_URL, {string: "yep", number: 1});

        generatedUrl.should.containEql("string=yep");
        generatedUrl.should.containEql(TEST_URL);
        generatedUrl.should.containEql("number=1");
        done();
      })
      .catch(done);
  });

  it("Should perform a regular fetch-based JSON POST request", (done) => {
    System.import("./js/util")
      .then(util => {
        util.jsonPOSTRequest(TEST_URL, POST_TEST_PARAMS)
          .then((res) => {
            res.url.should.be.exactly(TEST_URL);
            should.exist(res.opts.body);
            should.exist(res.opts.headers);
            res.opts.headers['Content-Type'].should.containEql('application/json');
            res.opts.headers['Accept'].should.containEql('application/json');
            res.opts.body.should.be.exactly(JSON.stringify(POST_TEST_PARAMS));
            done();
          })
          .catch(done);
      })
      .catch(done);
  });

  it("Should properly parse a location hash for a unsuccessful FB login", (done) => {
    System.import("./js/util")
      .then(util => {
        let result = util.parseLocationHashForLoginResp(FAILURE_LOC_HASH);
        result.should.be.eql({
          code: "400",
          type: "bad",
          desc: "test error",
          message: "testing"
        });
        done();
      })
      .catch(done);
  });

  it("Should properly parse a location hash for a successful FB login", (done) => {
    System.import("./js/util")
      .then(util => {
        let result = util.parseLocationHashForLoginResp(SUCCESS_LOC_HASH);
        result.should.be.eql({
          accessToken: "1234abcdefg",
          expiresIn: "146000"
        });
        done();
      })
      .catch(done);
  });

  it("Should fail to parse a location hash that fails to conform", (done) => {
    System.import("./js/util")
      .then(util => {
        let result = util.parseLocationHashForLoginResp("NOPE");
        should.not.exist(result);
        done();
      })
      .catch(done);
  });

  it("Should be optimistic if somehow both indicators for success and failure parsed from location hash", (done) => {
    System.import("./js/util")
      .then(util => {
        let result = util.parseLocationHashForLoginResp(FAILURE_AND_SUCCESS_LOC_HASH);
        result.should.be.eql({
          accessToken: "1234abcdefg",
          expiresIn: "146000"
        });
        done();
      })
      .catch(done);
  });

  it("Should generate a function that sets boolean state for a React component", (done) => {
    System.import("./js/util")
      .then(util => {
        let prop = "test";
        let comp = {
          state: {test: false},
          setState: sinon.spy()
        };

        // Generate the state toggling function
        let fn = util.toggleStateBoolean(prop, comp);
        fn.should.be.a.Function();

        // Toggle back and forth
        fn();
        comp.setState.should.be.calledWithExactly({test: true}); // only called once so far
        comp.state.test = true;

        fn();
        comp.setState.should.be.calledWithMatch({test: false}); // called another time, so have to use Match
        comp.state.test = false;

        comp.setState.should.be.calledTwice();
        done();
      })
      .catch(done);
  });

  it("Should reject an invalid tinder user preferences object", (done) => {
    System.import("./js/util")
      .then(util => {
        util.isValidTinderUserPreferencesObject({}).should.be.false();
        util.isValidTinderUserPreferencesObject(INVALID_TINDER_USER_PREFS.INCOMPLETE).should.be.false();
        util.isValidTinderUserPreferencesObject(INVALID_TINDER_USER_PREFS.INVALID_MIN).should.be.false();
        done();
      })
      .catch(done);
  });

  it("Should accept a valid tinder user preferences object", (done) => {
    System.import("./js/util")
      .then(util => {
        util.isValidTinderUserPreferencesObject(VALID_TINDER_USER_PREF_OBJ).should.be.true();
        done();
      })
      .catch(done);
  });

});

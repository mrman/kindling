import System from "systemjs";
import "../config";
import "should";
import _ from "lodash";

const EXPECTED_ACTIONS = [
  "authenticateWithTinder",
  "changeLocaleByCode",
  "error",
  "getCurrentRec",
  "getNextTinderRecommendation",
  "like",
  "loggedIn",
  "login",
  "logout",
  "matchResultDetected",
  "outOfLikes",
  "pass",
  "pullTinderRecommendations",
  "receivedSentMessageReceipt",
  "republishTinderUser",
  "retrieveMatchedUserInfo",
  "retrieveTinderUpdates",
  "sendMessageToMatch",
  "sentimentSuccessfullyProcessed",
  "successfullyDeletedMatch",
  "updateLocationFromGPS",
  "updateTinderPreferences"
];

describe("React actions", () => {

  // Ensure that the generated actions export contains the functions we expect
  it("Should contain all the actions we expect", (done) => {
    System.import("./js/actions")
      .then(actions => {
        actions.default.should.have.keys(EXPECTED_ACTIONS);
        done();
      })
      .catch(done);
  });

});

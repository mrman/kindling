export default {
  match:{
    _id:"556f8b926c01ba786888dcdd5647e40a046544192cb1sk12",
    closed:false,
    common_friend_count:0,
    common_like_count:0,
    created_date: "2016-05-21T06:31:16.046Z",
    dead:false,
    last_activity_date:"2016-05-21T06:31:16.046Z",
    message_count:0,
    messages:[],
    participants:["54bb13d1cdc25bab190ee607","54bb13d1cdc25bab190ee607"],
    pending:false,
    is_super_like:false,
    following:true,
    following_moments:true
  },
  likes_remaining:100
};

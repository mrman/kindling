export default {
  distance_mi:3000000,
  connection_count:405,
  common_like_count:100,
  common_friend_count:0,
  common_likes:[],
  common_friends:[],
  _id:"54bb13d1cdc25bab190ee607",
  bio:"When I'm not battling enemies in outer space, I love to snuggle up and watch movies!",
  birth_date:"1989-01-15T00:00:00.000Z",
  gender:1,
  name:"Samus Aran",
  ping_time:"2015-12-26T21:01:37.101Z",
  photos:[
    { 
      id: "091f0306-2be7-4520-842b-e01e49ab8ab4",
      url:"/public/images/test/fixtures/female-user/pic-1/original.jpg",
      processedFiles: [
        { 
          width:640,
          height:640,
          url:"/public/images/test/fixtures/female-user/pic-1/640x640.jpg"
        },
        {
          width:320,
          height:320,
          url:"/public/images/test/fixtures/female-user/pic-1/320x320.jpg"
        },
        {
          width:172,
          height:172,
          url:"/public/images/test/fixtures/female-user/pic-1/172x172.jpg"
        },
        {
          width:84,
          height:84,
          url:"/public/images/test/fixtures/female-user/pic-1/84x84.jpg"
        }
      ]
    },
    { 
      id: "091f0306-2be7-4520-842b-e01e49ab8ab4",
      url:"/public/images/test/fixtures/female-user/pic-2/original.jpg",
      processedFiles: [
        { 
          width:640,
          height:640,
          url:"/public/images/test/fixtures/female-user/pic-2/640x640.jpg"
        },
        {
          width:320,
          height:320,
          url:"/public/images/test/fixtures/female-user/pic-2/320x320.jpg"
        },
        {
          width:172,
          height:172,
          url:"/public/images/test/fixtures/female-user/pic-2/172x172.jpg"
        },
        {
          width:84,
          height:84,
          url:"/public/images/test/fixtures/female-user/pic-2/84x84.jpg"
        }
      ]
    }
  ],
  jobs:[
    { 
      company:
      { name: "Some company"},
      title: { name:"Boss" }
    }
  ],
  schools:[
    { 
      name:"University of texas austin",
      id:"111944028832853"
    }
  ],
  teaser:{
    string:"Doing the usual stuff",
    type:"jobPosition"
  },
  birth_date_info: "fuzzy birthdate active, not displaying real birth_date"
};

export default {
  EMPTY: {
    matches: [],
    blocks: [],
    lists: [],
    deleted_lists: [],
    last_activity_date: "2015-11-15T01:46:49.227Z"
  }
};

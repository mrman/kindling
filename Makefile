.PHONY: all test test-e2e clean package

all: bundle jspm-css ffos-manifest public-files index-build build-completed-msg

build-completed-msg:
	echo -e "\n----- Build Completed -----\n"

bundle:
	jspm bundle-sfx js/main dist/build.js

jspm-css: jspm-css-fontawesome jspm-css-pure

jspm-css-fontawesome:
	mkdir -p dist/jspm_packages/bower
	cp -r jspm_packages/bower/fontawesome* dist/jspm_packages/bower/
jspm-css-pure:
	mkdir -p dist/jspm_packages/bower
	cp -r jspm_packages/bower/pure* dist/jspm_packages/bower/

ffos-manifest:
	cp manifest.webapp dist/

index-build:
	cp index-build.html dist/index.html

public-files:
	cp -r public dist/

watch: clean watch-js-and-css

watch-js-and-css:
	find .  -not -path "./node_modules/*" \
		-not -path "./jspm_packages/*" \
		-type f \
		-name "*.js" -o -name "*.css" \
		| entr make

setup-dev:
	cd .git/hooks && \
		ln -s ../../pre-commit.sh pre-commit

test:
	npm test

test-e2e:
	npm run test-e2e

package:
	cd dist && \
	zip -r kindling.zip *

clean:
	rm -rf dist/*

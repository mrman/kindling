import _ from "lodash";
import enUS from "./i18n/languages/en-US";
import jaJP from "./i18n/languages/ja-JP";
import esES from "./i18n/languages/es-ES";

const KEYPATH_INVALID_MESSAGE = "INVALID KEYPATH: ";

export const SUPPORTED_LOCALES = {
  "en-US": {
    code: "en-US",
    dictionary: enUS,
    name: "English (USA)",
    nameInLocale: "English (US)",
    flagIconClass: "flag-icon-us"
  },

  "ja-JP": {
    code: "ja-JP",
    dictionary: jaJP,
    name: "Japanese (JAPAN)",
    nameInLocale: "日本語",
    flagIconClass: "flag-icon-jp"
  },

  "es-ES": {
    code: "es-ES",
    dictionary: esES,
    name: "Spanish",
    nameInLocale: "Español",
    flagIconClass: "flag-icon-es"
  }

};

export function getLocaleData(locale="en-US") {
  return SUPPORTED_LOCALES[locale];
}

export function isValidLocaleCode(code) {
  return _.has(SUPPORTED_LOCALES, code);
}

export function generateI18NTemplateFn(localeCode) {
  return (args, ...values) => {
    let translations = SUPPORTED_LOCALES[localeCode].dictionary;
    let keypath = _.first(args).trim();
    let translated = _.get(translations, keypath);

    // Exit early if translation failed
    if (_.isNull(translated)) {
      return `${KEYPATH_INVALID_MESSAGE} [${keypath}]`;
    }

    // Add in provided values if present
    translated = _.reduce(values, (acc,v) => acc.replace("{}", v), translated);

    return translated;
  };
}

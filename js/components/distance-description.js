import React from "react";
import LocaleStore from "../stores/locale";
import _ from "lodash";

class DistanceDescription extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};
    this.state = {
      i18n: LocaleStore.state.i18nTemplateFn,
      distance: props.distance || 0
    };
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  render() {
    // Determine which icon to use
    let distanceIconClass = "fa-bicycle";
    if (this.state.distance > 1000) {
      distanceIconClass = "fa-plane";
    } else if (this.state.distance > 10) {
      distanceIconClass = "fa-car";
    } else if (this.state.distance > 5) {
      distanceIconClass = "fa-cab";
    }

    // Determine how to express units
    let unit = (this.state.distance == 1) ? this.state.i18n`distances.mile.single` : this.state.i18n`distances.mile.multiple`;

    return (<span><i className={`fa ${distanceIconClass}`}></i> {this.state.i18n`distances.unitsAway ${this.state.distance} ${unit}`}</span>);
  }
}

export default DistanceDescription;

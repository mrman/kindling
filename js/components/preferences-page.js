import React from "react";
import Actions from "../actions";
import LHSNav from "./lhs-nav";
import LocaleStore from "../stores/locale";
import LocaleSelectBox from "./locale-select-box";
import TinderUserStore from "../stores/tinder-user";
import CollapsibleLabeledFormSection from "./collapsible-labeled-form-section";
import Constants from "../constants";
import _ from "lodash";

const DEFAULT_STATE = { prefs: null, useCurrentGPS: true };

class PreferencesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = DEFAULT_STATE;
    this.unsubFunctions = {};

    // Add i18n default templating function
    this.state = _.extend(this.state, {i18n: LocaleStore.state.i18nTemplateFn});

    // Set prefs if user is available through the TinderUserStore
    if (_.isNull(this.state.prefs) && !_.isNull(TinderUserStore.state.user)) {
      let user = _.clone(TinderUserStore.state.user, true);
      let prefs = this.buildPreferencesFromUser(user);
      this.state = _.extend(this.state, {prefs});

      // Set relevant state from user
      this.state = _.extend(this.state, {prefs: this.buildPreferencesFromUser(user)});
    }

    console.log("[PREFERENCES PAGE COMPONENT] Preference page constructed, initial state:", this.state);
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      console.log("[PREFERENCES PAGE COMPONENT] Received locale update!", locale);
      this.setState({i18n: locale.i18nTemplateFn});
    });

    this.unsubFunctions["store-tinder-user"] = TinderUserStore.listen(store => {
      console.log("[PREFERENCES PAGE COMPONENT] Detected tinder user update, updating state");
      this.setState({prefs: this.buildPreferencesFromUser(store.user)});
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  /**
   * Update the modified user contained in the state of the page
   *
   * @param {Object} user - user object from which to pull preferences
   */
  buildPreferencesFromUser(user) {
      // Set relevant state from user
    return _.extend({}, {
        full_name: user.full_name,
        gender: user.gender,
        distance_filter: user.distance_filter,
        age_filter_min: user.age_filter_min,
        age_filter_max: user.age_filter_max
    });
  }

  updatePreferences() {
    console.log("[PREFERENCES PAGE] Updating preference!");
    Actions.updateTinderPreferences(this.state.prefs);

    if (this.state.useCurrentGPS) {
      this.doUpdateLocationFromGPS();
    }
  }

  resetPreferences() {
    console.log("[PREFERENCES PAGE] Resetting preference information on form...");
    Actions.republishTinderUser();
  }

  updateAgeFilterMin(e) {
    let newValue = _.parseInt(e.target.value);
    console.log(`[PREFERENCES PAGE] Age filter minimum changed to ${newValue}`);
    this.setState({prefs: _.extend(this.state.prefs, {age_filter_min: newValue})});
  }

  updateAgeFilterMax(e) {
    let newValue = _.parseInt(e.target.value);
    console.log(`[PREFERENCES PAGE] Age filter maximum changed to ${newValue}`);
    this.setState({prefs: _.extend(this.state.prefs, {age_filter_max: newValue})});
  }

  updateDistanceFilter(e) {
    let newValue = _.parseInt(e.target.value);
    console.log(`[PREFERENCES PAGE] Distance filter minimum to ${newValue}`);
    this.setState({prefs: _.extend(this.state.prefs, {distance_filter: newValue})});
  }

  doUpdateLocationFromGPS() {
    console.log("[PREFERENCES PAGE COMPONENT] Setting location of user");
    Actions.updateLocationFromGPS(true);
  }

  /**
   * Change current page-state gender
   *
   * @param {number} gender - This is the tinder representation of the gender of the user (0 for male, 1 for female as of dec 2015)
   */
  genderFn(gender) {
    return () => {
      console.log(`[PREFERENCES PAGE] Gender filter changed to ${gender}`);
      this.setState({prefs: _.extend(this.state.prefs, {gender: gender})});
    };
  }

  render() {
    let validAges = _.range(Constants.TINDER_MIN_FILTER_AGE, Constants.TINDER_MAX_FILTER_AGE + 1);
    let ageOptions = _.map(validAges, age => (<option key={age} value={age}>{age}</option>));

    let genderFilter = _.get(this.state, "prefs.gender");
    let genderIsMale = genderFilter === Constants.TINDER_GENDER_MALE;
    let genderIsFemale = genderFilter === Constants.TINDER_GENDER_FEMALE;
    let genderName = genderIsMale ? this.state.i18n`general.firstCapitalizedMale` : this.state.i18n`general.firstCapitalizedFemale`;

    // Request user to build prefs if not provided yet
    if (_.isNull(this.state.prefs)) {
      Actions.republishTinderUser();
    }

    let distanceFilter = _.get(this.state, "user.distance_filter", 0);
    let ageFilter = {
      min: _.get(this.state, "user.age_filter_min", 18),
      max: _.get(this.state, "user.age_filter_max", 35)
    };

    return (
<div>
  <LHSNav />
  <div className="preference-page-container contains-page-bottom-controls">
    <div className="pure-g">

      <div className="pure-u-1">
        <div className="right-aligned-text">
          <h1 className="no-vertical-margins slim-text">{this.state.i18n`nav.sections.preferences`}</h1>
          <h3 className="no-vertical-margins super-slim-text">{this.state.i18n`pages.preferences.pageSubtitle`}</h3>

          <form className="pure-form pure-form-stacked preference-update-form left-aligned-text"
                onSubmit={this.updatePreferences.bind(this)}>
            <fieldset>

              <CollapsibleLabeledFormSection labelFor="gender"
                                             labelText={this.state.i18n`pages.preferences.genderSectionHeading`}>
                <div className="pure-g">
                  <div className="pure-u-1-3">
                  <div className="two-button-options">

                    <button type="button"
                            id="gender-male"
                            className={`pure-button white-face ${genderIsMale ? "pure-button-disabled" : ""}`}
                            onClick={this.genderFn(Constants.TINDER_GENDER_MALE).bind(this)}>
                      <i className="fa fa-male"></i>
                    </button>

                    <button type="button"
                            id="gender-female"
                            className={`pure-button white-face ${genderIsFemale ? "pure-button-disabled" : ""}`}
                            onClick={this.genderFn(Constants.TINDER_GENDER_FEMALE).bind(this)}>
                      <i className="fa fa-female"></i>
                    </button>

                  </div>
                  </div>
                  <span className=" xlarge pure-u-2-3">{genderName}</span>
                </div>
              </CollapsibleLabeledFormSection>

              <CollapsibleLabeledFormSection labelFor="age-filter"
                                             labelText={this.state.i18n`pages.preferences.ageSectionHeading`}>
                <div className="pure-g">
                  <div className="pure-u-1-3 center-aligned-text">
                    <select id="age-filter-min"
                            className="full-width center-aligned-text"
                            onChange={this.updateAgeFilterMin.bind(this)}
                            value={this.state.prefs.age_filter_min}>
                      {ageOptions}
                    </select>
                  </div>
                  <div className="pure-u-1-3 center-aligned-text hacked-vertical-center">
                    <span className="large">{this.state.i18n`pages.preferences.inbetweenAges`}</span>
                  </div>
                  <div className="pure-u-1-3 center-aligned-text">
                    <select id="age-filter-max"
                            className="full-width center-aligned-text"
                            onChange={this.updateAgeFilterMax.bind(this)}
                            value={this.state.prefs.age_filter_max}>
                      {ageOptions}
                    </select>
                  </div>
                </div>
              </CollapsibleLabeledFormSection>

              <CollapsibleLabeledFormSection labelFor="distance-filter"
                                             labelText={this.state.i18n`pages.preferences.distanceSectionHeading`}>
                <div className="pure-g">
                  <div className="pure-u-1-2">
                    <input id="distance-filter"
                           type="number"
                           className="full-width"
                           onChange={this.updateDistanceFilter.bind(this)}
                           value={this.state.prefs.distance_filter}/>
                  </div>
                  <div className="pure-u-1-2 hacked-vertical-center">
                    <span className="large">{this.state.i18n`distances.milesAway`}</span>
                  </div>
                </div>
              </CollapsibleLabeledFormSection>


              <CollapsibleLabeledFormSection labelFor="location-gps"
                                             labelText={this.state.i18n`pages.preferences.locationSectionHeading`}>
                <div className="pure-g">
                  <div className="row">
                    <button type="button"
                            onClick={() => this.setState({useCurrentGPS: true})}
                            className="pure-u-1-2 pure-button squared button-secondary">
                      <i className="fa fa-map-marker"></i> {this.state.i18n`pages.preferences.fromGPS`}
                    </button>

                    <button type="button"
                            className="pure-u-1-2 pure-button squared pure-button-disabled">
                      <i className="fa fa-pencil"></i> {this.state.i18n`pages.preferences.customLocation`}
                    </button>
                  </div>
                </div>
              </CollapsibleLabeledFormSection>

            </fieldset>
          </form>
        </div>
      </div> {/* /.pure-u-1 */}

      <div className="pure-u-1">
            <LocaleSelectBox />
      </div>

      <div className="pinned-to-bottom page-bottom-controls">
        <div className="pure-g">

          <div className="pure-u-1-2">
            <button type="submit"
                    className="pure-u-1 pure-button button-success button-lg squared"
                    onClick={this.updatePreferences.bind(this)}>
              <div><i className="fa fa-save"></i></div>
              <div className="center-aligned-text small-text">{this.state.i18n`pages.preferences.updatePreferences`}</div>
            </button>
          </div>

          <div className="pure-u-1-2">
            <button type="button"
                    className="pure-u-1 pure-button button-lg squared"
                    onClick={this.resetPreferences.bind(this)}>
              <div><i className="fa fa-trash"></i></div>
              <div className="center-aligned-text small-text">{this.state.i18n`pages.preferences.undoChanges`}</div>
            </button>
          </div>

        </div>
      </div>

    </div>
  </div>
</div>
    );
  }
}

export default PreferencesPage;

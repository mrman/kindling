import React from "react";
import { PropTypes } from "react-router";

class LHSNav extends React.Component {

  goHome() { this.context.history.pushState(null, '/login');  }

  render() {
    // Figure out if back button should appear
    let back;
    if (this.props.backEnabled) {
      back = (<i className="fa fa-arrow-left xlarge picture-overlay-text"
                 onClick={this.context.history.goBack}></i>);
    }

    return (
<div className="lhs-nav pure-u-1-8">
  {back}

  <i className="fa fa-home xlarge picture-overlay-text"
     onClick={this.goHome.bind(this)}></i>

  {this.props.children}
</div>
    );
  }
}

// Add mixins
LHSNav.contextTypes = {
  history: PropTypes.history
};

export default LHSNav;

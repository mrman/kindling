import React from "react";
import Actions from "../actions";
import LocaleStore from "../stores/locale";
import { isValidLocaleCode, getLocaleData } from "../i18n";
import _ from "lodash";

class LocaleSelectBox extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};

    this.state = {
      i18n:  LocaleStore.state.i18nTemplateFn,
      supportedLocales: LocaleStore.state.supportedLocales,
      defaultSelectedLocaleCode: LocaleStore.state.defaultLocale,
      selectedLocale: LocaleStore.state.defaultLocale
    };

    // Set the selected locale to the current locale LocaleStore is using, if set
    console.log("LocaleStore.state.locale?", LocaleStore.state.locale);
    if (LocaleStore.state.locale) {
      this.state.defaultSelectedLocaleCode = LocaleStore.state.locale.code;
    }
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({
        selectedLocale: locale.locale,
        i18n: locale.i18nTemplateFn
      });

    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  changeLocaleByCode(e) {
    let code = e.target.value;
    if (isValidLocaleCode(code)) {
      console.log(`[LOCALE SELECT BOX] Changing locales to locale with code [${code}]`);
      Actions.changeLocaleByCode(code);
    }
  }

  render() {
    let options = _(this.state.supportedLocales)
          .values()
          .sortBy('name')
          .map(l => {
            return (
                <option className={`locale-option ${l.localeClassName}`} key={l.code} value={l.code}>
                {l.nameInLocale}
              </option>
            );
          })
          .value();

    return (
<div className="locale-select-box-component">
  <div className="pure-g">

    <div className="pure-u-1">
        <h3 className="no-margin">{this.state.i18n`general.firstCapitalizedLanguage`}</h3>
    </div>

<div className="pure-u-1">
        <label htmlFor="locale-select" className="pure-u-1-6"><span className={`flag-icon ${this.state.selectedLocale.flagIconClass}`}></span></label>
        <select id="locale-select"
                className="pure-u-5-6"
                onChange={this.changeLocaleByCode.bind(this)}
                defaultValue={this.state.defaultSelectedLocaleCode}>
          {options}
        </select>
</div>

  </div>
</div>
    );
  }
}

export default LocaleSelectBox;

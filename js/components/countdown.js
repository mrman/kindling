import React from "react";
import LocaleStore from "../stores/locale";
import moment from "moment";
import _ from "lodash";


class Countdown extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};
    this.state = {
      i18n: LocaleStore.state.i18nTemplateFn,
      date: props.date || null,
      updateIntervalMs: 1000,
      now: new Date(),
      intervalId: null
    };

    // Set locale with locale store
    var locale = LocaleStore.state.locale;
    moment.locale(locale.code || "en"); // Set default locale
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });

    this.setState({
      intervalId: window.setInterval(() => this.setState({now: new Date()}), this.state.updateIntervalMs)
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
    window.clearInterval(this.state.intervalId);
  }

  render() {
    // Return "Unknown if date was not provided/is invalid
    if (_.isNull(this.state.date)) { return (<span>Unknown</span>);  }

    let now = moment(this.state.now);
    let then = moment(this.state.date);
    let diffHours = then.diff(now, 'hours');

    // Add the hours to get just minutes
    let nowPlusHours = now.add(diffHours, 'hours');
    let diffMinutes = then.diff(nowPlusHours, 'minutes');

    // Add the minutes to get just seconds
    let nowPlusMins = now.add(diffMinutes, 'minutes');
    let diffSeconds = then.diff(nowPlusMins, 'seconds');

    return (
<span>
  <span>{this.state.i18n`time.xHours ${diffHours}`}</span>&nbsp;&nbsp;
  <span>{this.state.i18n`time.xMinutes ${diffMinutes}`}</span>&nbsp;&nbsp;
  <span>{this.state.i18n`time.xSeconds ${diffSeconds}`}</span>&nbsp;&nbsp;
</span>
    );
  }
}

export default Countdown;

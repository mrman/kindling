import _ from "lodash";
import React from "react";
import { PropTypes } from "react-router";
import FBAuthStore from "../stores/fb-auth";
import Actions from "../actions";
import { parseLocationHashForLoginResp } from "../util";

class ProcessLoginPage extends React.Component {
  constructor(props, context) {
    super(props);
    console.log("[PROCESS LOGIN PAGE COMPONENT] Initializing ProcessLoginPage!");

    // Detect if the page was loaded as a result of a FB login
    let result = parseLocationHashForLoginResp(location.hash);
    if (result != null && 'accessToken' in result) {
      console.log("[PROCESS LOGIN PAGE COMPONENT], Detected valid access token in URL, triggering [logged in] action.");
      Actions.loggedIn(result);
    }
  }

  componentDidMount() {
    this.unsubscribe = FBAuthStore.listen(auth => {
      // Check if the user has been logged in
      // This generally happens on first login (as opposed to similar handling code in login-page),
      // when the app is completely redirected and the user comes IN with credentials attached to the route.
      if (auth.loggedIn) {
        console.log("[PROCESS LOGIN PAGE COMPONENT] Detected that user has been logged in, proceeding to app");
        this.context.history.pushState(null, '/app');
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    return (
      <div className="row">
        <h2>Processing Facebook Login...</h2>
        <h3>This process should only take less than 5 seconds.</h3>
        <h3>If experiencing issues, please restart the app.</h3>
      </div>
    );
  }

}

// Add mixins
ProcessLoginPage.contextTypes = {
  history: PropTypes.history
};

export default ProcessLoginPage;

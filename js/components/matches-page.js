import React from "react";
import { PropTypes } from "react-router";
import Actions from "../actions";
import LHSNav from "./lhs-nav";
import LocaleStore from "../stores/locale";
import TinderMatchStore from "../stores/tinder-match";
import TinderUserStore from "../stores/tinder-user";
import MatchConvoLink from "./match-convo-link";
import MatchConvoPage from "./match-convo-page";
import Constants from "../constants";
import _ from "lodash";

const DEFAULT_STATE = { matches: [], matchedUserInfo: {}, selectedMatch: null, user: null};

class MatchesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = _.extend(DEFAULT_STATE, {i18n: LocaleStore.state.i18nTemplateFn});
    this.unsubFunctions = {};

    // Set matches if user is available through the TinderMatchStore
    if (_.isNull(this.state.matches) && !_.isNull(TinderMatchStore.state.user)) {
      let matches = _.clone(TinderMatchStore.state.matches, true);
      this.state = _.extend(this.state, {matches});
    }

    // Set tinder user if user is available through the TinderUserStore
    if (_.isNull(this.state.user) && !_.isNull(TinderUserStore.state.user)) {
      let user = _.clone(TinderUserStore.state.user, true);
      this.state = _.extend(this.state, {user});
    }

    console.log("[MATCHES PAGE COMPONENT] Match page constructed, initial state:", this.state);
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });

    this.unsubFunctions["store-tinder-match"] = TinderMatchStore.listen(store => {
      console.log("[MATCHES PAGE COMPONENT] Detected tinder match store update", store);
      this.setState({matches: store.matches, matchedUserInfo: store.matchedUserInfo});
    });

    this.unsubFunctions["store-tinder-user"] = TinderUserStore.listen(store => {
      console.log("[MATCHES PAGE COMPONENT] Detected tinder user store update", store);
      this.setState({user: store.user});
    });

    // Attempt to fetch updates from twitter whenever matches page loads,
    // this will trigger the match store to have updates
    Actions.retrieveTinderUpdates();
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions, f => f());
  }

  showMatchChatPage(id) {
    console.log("[MATCHES PAGE] Opening chat page for match with ID ", id);
  }

  hideMatchConvoContainer() {
    console.log("[MATCHES PAGE] Hiding match convo container...");
    this.setState({selectedMatch: null});
  }

  gotoRecsPage() { this.context.history.pushState(null, '/recs'); }

  showMatchConvoContainerWithMatch(m) {
    console.log("[MATCHES PAGE] Showing convo container for match:", m);
    this.setState({selectedMatch: m});
  }

  render() {
    let matchCount = _.size(this.state.matches);
    console.log(`[MATCHES PAGE] Rendering matches page with ${matchCount} matches`);

    // Generate elemtn that will list or show empty message
    let matchListElem;
    if (_.isEmpty(this.state.matches)) {
      // Generate no matches message
      matchListElem = (
<div id="no-matches-message" className="pure-u-1 center-aligned-text">
  <h3 className="slim-text">{this.state.i18n`pages.matches.noMatchesFound`}</h3>
  <button onClick={this.gotoRecsPage.bind(this)}
          className="pure-button button-success">
    <i className="fa fa-binoculars"></i> {this.state.i18n`pages.matches.findSomeoneNew`}
  </button>
</div>
      );

    } else {
      // Generate list of matches
      let matchLinks = _.map(this.state.matches, m => {
        let unseenByUserMessageCount = _.reduce(m.messages, (acc, m) => !m.seenByUser ? acc+1 : acc , 0);
        let lastMessageSentDate = _.get(_.last(m.messages), "sent_date", null);
        return (
<div key={m.id} onClick={() => this.showMatchConvoContainerWithMatch(m)} >
  <MatchConvoLink unseenByUserMessageCount={unseenByUserMessageCount}
                  matchId={m.id}
                  matchedUserId={m.matchedUserId}
                  lastMessageSentDate={lastMessageSentDate} />
</div>
        );
      });

      matchListElem = (
<div className="pure-u-1">
  {matchLinks}
</div>
      );

    }

    let convoShown = _.isNull(this.state.selectedMatch) ? "" : "onscreen";
    let selectedMatch = this.state.selectedMatch;
    let matchedUser = _.isNull(selectedMatch) ? {} : _.get(this.state.matchedUserInfo, this.state.selectedMatch.id);
    let viewingUser = this.state.user;

    return (
<div className="matches-page-container">
  <LHSNav />
  <div>
    <div className="pure-g">
      <div className="pure-u-1 right-aligned-text">
        <div className="right-aligned-text">
          <h1 className="no-vertical-margins slim-text">{this.state.i18n`nav.sections.matches`}</h1>
          <h3 className="no-vertical-margins super-slim-text">{this.state.i18n`pages.matches.pageSubtitle`}</h3>
        </div>
      </div>
    </div>

    <div className="pure-g">
        {matchListElem}
    </div>
  </div>

  <div id="match-convo-sliding-container" className={`offscreen-right ${convoShown}`}>
    <MatchConvoPage match={selectedMatch}
                    matchedUser={matchedUser}
                    viewingUser={viewingUser}
                    showCloseButton={true}
                    closeFn={this.hideMatchConvoContainer.bind(this)}/>
  </div>
</div>
    );
  }
}

// Add mixins
MatchesPage.contextTypes = {
  history: PropTypes.history
};

export default MatchesPage;

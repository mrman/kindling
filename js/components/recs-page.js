import React from "react";
import { Link, PropTypes } from "react-router";
import LocaleStore from "../stores/locale";
import TinderRecommendationStore from "../stores/tinder-recommendation";
import TinderSentimentStore from "../stores/tinder-sentiment";
import LHSNav from "./lhs-nav";
import GenderIcon from "./gender-icon";
import DistanceDescription from "./distance-description";
import MomentFuzzyDate from "./moment-fuzzy-date";
import Countdown from "./countdown";
import PictureStrip from "./picture-strip";
import Actions from "../actions";
import _ from "lodash";

const DEFAULT_STATE = {recPhotoIndex: 0, rec: null, detailVisible: true, outOfLikes: false, outOfLikesUntil: 0};

class RecsPage extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};
    this.state = _.extend(DEFAULT_STATE, {i18n: LocaleStore.state.i18nTemplateFn});

    // Set rec if available through the TinderRecommendationStore
    if (_.isNull(this.state.rec) && !_.isNull(TinderRecommendationStore.state.currentRec)) {
      this.state.rec = _.get(TinderRecommendationStore, "state.currentRec", null);
      this.state.recsFetchFailed = _.get(TinderRecommendationStore, "state.recsFetchFailed", false);
    }

    // Set outOfLiekes & outOfLikesUntil if available through the TinderRecommendationStore
    this.state.outOfLikes = _.get(TinderSentimentStore, "state.outOfLikes", false);
    this.state.outOfLikesUntil = _.get(TinderSentimentStore, "state.outOfLikesUntil", 0);

  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });

    // Setup subscriptions
    this.unsubFunctions["store-tinder-recommendations"] = TinderRecommendationStore.listen(state => {
      // Update current recommendation
      console.log("[RECS PAGE COMPONENT] Detected rec store update, updating according to state:", state);
      this.setState({rec: state.currentRec, recsFetchFailed: state.recsFetchFailed});
    });

    // Detect state changes in sentiment store
    this.unsubFunctions["store-tinder-sentiment"] = TinderSentimentStore.listen(state => {
      console.log("[RECS PAGE COMPONENT] Detected sentiment store update, updating according to state:", state);
      this.setState({outOfLikes: state.outOfLikes, outOfLikesUntil: state.outOfLikesUntil});
    });

    // Get the current rec
    Actions.getCurrentRec();
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  toggleDetail() { this.setState({detailVisible: !this.state.detailVisible}); }

  pass() { Actions.pass(this.state.rec); }
  like() { Actions.like(this.state.rec); }

  pullRecommendations() { Actions.pullTinderRecommendations(); }

  render() {
    // If there is no current rec, request some
    if (_.isNull(this.state.rec)) { this.pullRecommendations(); }

    // Return early if user is out of likes
    let outOfLikes = this.state.outOfLikes && ((new Date().getTime()) < this.state.outOfLikesUntil);
    let outOfLikesUntilDate = new Date(this.state.outOfLikesUntil);
    if (outOfLikes) {
      return (
<div className="pure-g">
  <div className="pure-u-1 center-aligned-text">
    <h2>{this.state.i18n`pages.recs.outOfLikesHeading`}</h2>
    <h3>{this.state.i18n`pages.recs.outOfLikesMessage`}</h3>
  </div>

  <div className="pure-u-1 center-aligned-text">
    <h4>{this.state.i18n`pages.recs.likesReplenishInXHours`}</h4>
    <h3><Countdown date={outOfLikesUntilDate}/></h3>
  </div>

  <div className="pure-u-1 center-aligned-text">
    <button onClick={this.context.history.goBack} className="pure-button button-error">
      <i className="fa fa-arrow-left"></i> {this.state.i18n`nav.back`}
    </button>
  </div>

</div>
      );

    }


    let recsFetchFailedMessage = "";
    if (this.state.recsFetchFailed) {
        recsFetchFailedMessage = (
<div className="pure-u-1">
  <div className="alizarin-bg white-face slim-pad slim-margin">
    {this.state.i18n`pages.recs.recsFetchFailed`}
  </div>
</div>
        );
    }

    // Return early if there is no rec
    if (_.isNull(this.state.rec) || this.state.recsFetchFailed) {
      return (
<div className="pure-g">
  <div className="pure-u-1 center-aligned-text">
    <h3>{this.state.i18n`pages.recs.noRecsMessage`}</h3>
    <h4>{_.isNull(this.state.rec) && !this.state.recsFetchFailed ? this.state.i18n`pages.recs.updatePrefsMaybe` : this.state.i18n`pages.recs.hitDailyLimitMaybe`}</h4>
  </div>

  {recsFetchFailedMessage}

  <div className="pure-u-1">
    <button className="pure-button button-secondary in-stack">
      <i className="fa fa-wrench"></i> {this.state.i18n`pages.recs.managePrefs`}
    </button>
  </div>

  <div className="pure-u-1">
    <button onClick={this.context.history.goBack} className="pure-button button-error">
      <i className="fa fa-arrow-left"></i> {this.state.i18n`nav.back`}
    </button>
  </div>

</div>
      );
    }

    // Generate teaser fragment
    let teaserFragment = null;
    if (_.has(this.state.rec, "teaser.string")) {
      let teaserFragment = <p className="no-vertical-margins picture-overlay-text">{this.state.rec.teaser.string}</p>;
    }

    // Generate detail fragment
    let detailFragment = (
 <div className={`rec-detail picture-overlay-text ${this.state.detailVisible ? "visible" : "invisible"}`} >
   <p className="picture-overlay-text"><DistanceDescription distance={this.state.rec.distance_mi}/></p>
   <p className="picture-overlay-text"><i className="fa fa-users"></i> {this.state.i18n`pages.recs.friendsInCommon ${this.state.rec.common_friend_count}`}</p>
   <p className="picture-overlay-text"><i className="fa fa-thumbs-up"></i> {this.state.i18n`pages.recs.commonInterests ${this.state.rec.common_like_count}`}</p>
   <p className="picture-overlay-text"><i className="fa fa-clock-o"></i> {this.state.i18n`pages.recs.lastActive`} <MomentFuzzyDate date={this.state.rec.ping_time}/></p>
 </div>
    );

    // Generate optional bio fragment
    let bioFragment = this.state.detailVisible ? (<h3 className="picture-overlay-text no-margin">{this.state.rec.bio}</h3>) : null;

    return (
 <div className="full-height">

   <div className="recs-page-header">
        <div className="pure-g">
       <LHSNav backEnabled={false}>
         <i className="fa fa-info-circle xlarge picture-overlay-text"
            onClick={this.toggleDetail.bind(this)}></i>
       </LHSNav>
</div>

     <div className="pure-g">
       <div className="pure-u-1-4"></div>
       <div className="pure-u-3-4 right-aligned-text">
         <h1 className="picture-overlay-text no-margin">
           {this.state.rec.name} <GenderIcon gender={this.state.rec.gender}/>
         </h1>

         {bioFragment}
       </div>

     </div>
   </div>

   {teaserFragment}

   <PictureStrip photos={this.state.rec.photos} index={this.state.recPhotoIndex} orientation="vertical"/>

   {detailFragment}

   <div className="pinned-to-bottom page-bottom-controls">
     <div className="pure-g">
       <div className="pure-u-1-2">
         <button onClick={this.pass.bind(this)} className="pure-u-1 pure-button button-lg squared button-error">
           <div><i className="fa fa-binoculars"></i></div>
           <div className="center-aligned-text small-text">{this.state.i18n`pages.recs.keepSearching`}</div>
         </button>
       </div>

       <div className="pure-u-1-2">
         <button onClick={this.like.bind(this)} className="pure-u-1 pure-button button-lg squared button-success">
           <div><i className="fa fa-heart"></i></div>
           <div className="center-aligned-text small-text">{this.state.i18n`pages.recs.like`}</div>
         </button>
       </div>
     </div>
   </div>

 </div>
    );
  }
}

// Add mixins
RecsPage.contextTypes = {
  history: PropTypes.history
};

export default RecsPage;

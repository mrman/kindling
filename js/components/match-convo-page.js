import React from "react";
import Actions from "../actions";
import LocaleStore from "../stores/locale";
import MomentFuzzyDate from "./moment-fuzzy-date";
import _ from "lodash";
import { updatePropertyWithEventTargetValue } from "../util";

const DEFAULT_CLOSE_FN = () => console.log("[MATCH CONVO PAGE COMPONENT] Would have closed convo page!");
const DEFAULT_STATE = {messageText: ""};

class MatchConvoPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = _.extend(DEFAULT_STATE, {i18n: LocaleStore.state.i18nTemplateFn});
    this.unsubFunctions = {};

    console.log("[MATCH CONVO PAGE COMPONENT] Match convo link component constructed, initial state:", this.state);
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  sendMessageToMatch() {
    if (!_.has(this.props, "match")) {
      throw new Error("Match not provided for convo page");
    }

    // Gather message info
    let matchId = _.get(this.props, "match.id");
    let msg = this.state.messageText;

    console.log("[MATCH CONVO PAGE] Attempting to send message to match:", {
      messageText: msg,
      matchId: matchId
    });

    // Clear out the message text
    this.setState({messageText: ""});

    // Sent the message to the match
    Actions.sendMessageToMatch(matchId, msg);
  }

  render() {
    let messages = _.get(this.props, "match.messages", []);
    let pendingMessages = _.get(this.props, "match.pendingMessages", []);

    // Generate list of messages
    let messageListElements = messages.map((m,i) => {
      let receivedOrSent = m.to === this.props.viewingUser._id ? "received" : "sent";

      return (
<div key={m._id + '-msg-' + i} className="pure-u-1">
  <div className={`match-convo-message ${receivedOrSent}`}>
    <div className="match-convo-message slim-padding">{m.message}</div>
    <div className="match-convo-sent-date no-vertical-margins right-aligned-text slim-right-padding">
      <MomentFuzzyDate date={m.sent_date}/>
    </div>
  </div>
</div>
      );
    });

    // Generate list of pending messaegs
    let pendingMessageListElements = pendingMessages.map((m,i) => {
      return (
<div key={m._id + '-msg-' + i} className="pure-u-1">
  <div className={`match-convo-message pending`}>
    <div className="match-convo-message slim-padding">{m.message}</div>
    <div className="match-convo-sent-date no-vertical-margins right-aligned-text slim-right-padding">
      <i className="fa fa-circle-o-notch fa-spin"></i> {this.state.i18n`pages.matchConvo.sending`}
    </div>
  </div>
</div>
      );
    });

    let closeButton = "";
    if (this.props.showCloseButton) {
      closeButton = (
<div id="match-convo-close-btn"
     onClick={() => { if (this.props.closeFn) { this.props.closeFn(); }}}>
  <div className="close-icon-container center-aligned-text"><i className="fa fa-times large"></i></div>
</div>
      );
    }

    return (
<div className="match-convo-page-container">
  {closeButton}

  <div className="match-convo-page-messages-container">
    <div className="pure-g">
      {messageListElements}
      {pendingMessageListElements}
    </div>
  </div>

  <div className="match-convo-page-chat-box abs pinned-to-bottom full-width">
    <div className="pure-g full-height">

      <div className="pure-u-4-5 full-height">
        <textarea placeholder="Enter message..."
                  className="pure-u-1 full-height squared"
                  value={this.state.messageText}
                  onChange={updatePropertyWithEventTargetValue("messageText", this)}></textarea>
      </div>

      <div className="pure-u-1-5 full-height">
        <div className="full-height nephritis-bg center-aligned-text"
             onClick={this.sendMessageToMatch.bind(this)}>
          <i className="fa fa-send large white-face hacked-vertical-center"></i>
        </div>
      </div>

    </div>
  </div>

</div>
    );
  }
}

export default MatchConvoPage;

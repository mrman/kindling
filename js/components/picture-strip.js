import React from "react";
import _ from "lodash";

/**
 * Generate the img tag for a given picture
 *
 * @param {object} pic - The picture object, expected to at least contain the URL
 * @param {string} pic.url - The URL of the picture
 * @param {number} index - The index of the picture
 * @param {string} orientation - {"horizontal"|"vertical"}, should reflect whether to style images next to each other or stack them.
 * @returns A single DOM element representing the provided picture
 */
function generateImageTagForPictureInStrip(pic, index, orientation) {
  let url = pic.url;

  if (orientation === "horizontal") {
    // Setup some horizontal styles
    let style = {
      position: "absolute",
      left: `${index * 100}%`
    };

    // Attempt to find the largest image, if processFiles is available, and contains urls
    if (_.has(pic, "processedFiles")) {
      let largestProcessedPicture = _.reduce(pic.processedFiles, (acc, f) => {
        if (f.width > acc.width) { acc = {url: f.url, width: f.width}; }
        return acc;
      }, {url: url, width: 0});
      url = largestProcessedPicture && largestProcessedPicture.url ? largestProcessedPicture.url : url;
    }

    return <img key={pic.url} className="picture-strip-image" style={style} src={pic.url}/>;
  }

    return <img key={pic.url} className="picture-strip-image" src={pic.url}/>;
}

class PictureStrip extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let pictureDOMElements = _.map(this.props.photos, (pic, index) => generateImageTagForPictureInStrip(pic, index, this.props.orientation));

    let containerStyle = {};
    if (this.props.orientation === "horizontal") {
      containerStyle = {
        "position": "absolute",
        "overflow-x": "hidden"
      };
    }

    return (
      <div className="picture-strip-image-container" style={containerStyle}>
        {pictureDOMElements}
      </div>
    );
  }
}

export default PictureStrip;

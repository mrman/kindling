import React from "react";
import Actions from "../actions";
import LHSNav from "./lhs-nav";
import ErrorStore from "../stores/error";
import Constants from "../constants";
import _ from "lodash";

const DEFAULT_STATE = { errors: null };

class ErrorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = DEFAULT_STATE;
    this.unsubFunctions = {};

    // Load errors from error store if present
    console.log("error store:", ErrorStore);
    if (_.isNull(this.state.errors) && !_.isEmpty(ErrorStore.state.errors)) {
      this.state.errors = _.clone(ErrorStore.state.errors, true);
    }
    
    console.log("[ERROR PAGE COMPONENT] Error page constructed, initial state:", this.state);
  }

  componentDidMount() {
    this.unsubFunctions["store-error"] = ErrorStore.listen(store => {
      console.log("[ERRORS PAGE COMPONENT] Detected error store update");
      this.setState({errors: store.errors});
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions, f => f());
  }

  // Dismiss the current error (should be the first in the list)
  dismissCurrentError() {
    this.setState({errors: _.rest(this.state.errors)});
  }

  render() {

    if (_.isEmpty(this.state.errors)) {
      return (
<div>
  <LHSNav />
  <div className="error-page-container">
    <div className="pure-g">
      <div className="pure-u-1">
        <div className="right-aligned-text">
          <h1 className="no-vertical-margins slim-text alizarin-face">Error</h1>
          <h3 className="no-vertical-margins super-slim-text">Something's gone wrong</h3>
        </div>
      </div>
    </div>
    <div className="pure-g">
      <div className="pure-u-1">
        <h2>No errors detected!</h2>
      </div>
    </div>
  </div>
</div>
);
    }
    
    let currentError = _.first(this.state.errors);
    let errorTitle = currentError.title || "Error";
    let errorMsg = currentError.msg || "An unexpected error has occurred";

    return (
<div>
  <LHSNav />
  <div className="error-page-container">
    <div className="pure-g">
      <div className="pure-u-1">
        <div className="right-aligned-text">
          <h1 className="no-vertical-margins slim-text alizarin-face">Error</h1>
          <h3 className="no-vertical-margins super-slim-text">Something's gone wrong</h3>
        </div>
      </div>
    </div>

    <div className="pure-g">
      <div className="pure-u-1">
        <h2>{errorTitle}</h2>
        <h4>{errorMsg}</h4>
      </div>
    </div>
    
    <div className="pure-g">
      <div className="pure-u-1">
        <button type="button"
                className="pure-button button-success"
                onClick={this.dismissCurrentError.bind(this)}>
          <i className="fa fa-trash"></i> Dismiss Error
        </button>
      </div>
    </div>

  </div>
</div>
    );
  }
}

export default ErrorPage;

import React from "react";
import LocaleStore from "../stores/locale";
import moment from "moment";
import _ from "lodash";

class MomentFuzzyDate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: props.date || null};

    // Set locale with locale store
    var locale = LocaleStore.state.locale;
    moment.locale(locale.code || "en"); // Set default locale
  }

  render() {
    // Return "Unknown if date was not provided/is invalid
    if (_.isNull(this.state.date)) { return (<span>Unknown</span>);  }

    let fuzzyDate = moment(this.state.date).fromNow();

    return (<span>{fuzzyDate}</span>);
  }
}

export default MomentFuzzyDate;

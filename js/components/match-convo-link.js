import React from "react";
import TinderMatchStore from "../stores/tinder-match";
import LocaleStore from "../stores/locale";
import Actions from "../actions";
import MomentFuzzyDate from "./moment-fuzzy-date";
import _ from "lodash";

const DEFAULT_STATE = {
  matchId: null,
  matchedUser: null, // User that was matched to logged in user
  matchedUserId: null, // ID of the user that was matched to logged in user
  unseenByUserMessageCount: 0,
  lastMessageSentDate: null
};

class MatchConvoLink extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};

    // Process props
    this.state = DEFAULT_STATE;
    this.state = _.extend(this.state, {
      i18n: LocaleStore.state.i18nTemplateFn,
      matchId: props.matchId || this.state.matchId,
      matchedUserId: props.matchedUserId || this.state.matchedUserId,
      matchedUser: props.matchedUser || this.state.matchedUser,
      unseenByUserMessageCount: props.unseenByUserMessageCount || this.state.unseenByUserMessageCount,
      lastMessageSentDate: props.lastMessageSentDate || this.state.lastMessageSentDate
    });

    console.log("[MATCH CONVO LINK COMPONENT] Match convo link component constructed, initial state:", this.state);
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });

    this.unsubFunctions["store-tinder-match"] = TinderMatchStore.listen(store => {
      console.log("[MATCH CONVO LINK COMPONENT] Detected tinder match store update:", store);

      // Update the user info state if the user info is relevant to this component
      if (_.has(store.matchedUserInfo, this.state.matchedUserId)) {
        console.log(`[MATCH CONVO LINK COMPONENT] Found match ID ${this.state.matchedUserId} in update, updating state`);
        this.setState({matchedUser: store.matchedUserInfo[this.state.matchedUserId]});
      }
    });

    // Request a user's information if not provided
    if (!_.isNull(this.state.matchedUserId) && _.isNull(this.state.matchedUser)) {
      console.log(`[MATCH CONVO LINK COMPONENT] No user info for matched user with id ${this.state.matchedUserId}, requesting...`);
      Actions.retrieveMatchedUserInfo(this.state.matchedUserId);
    }
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  render() {
    console.log("[MATCH CONVO LINK COMPONENT] Rendering link for match with ID:", this.state.matchedUserId);

    let matchedUser = _.get(this.state, "matchedUser", {});

    let lastMessageSentFragment = "";
    if (_.isNull(this.state.lastMessageSentDate) || _.isUndefined(this.state.lastMessageSentDate)) {
      lastMessageSentFragment = this.state.i18n`pages.matches.noMessagesSent`;
    } else {
      lastMessageSentFragment = (<MomentFuzzyDate date={this.state.lastMessageSentDate}/>);
    }

    // TODO: show different view (progressive?) if user isn't loaded yet
    if (_.isNull(matchedUser)) { 
      return (
<div className="match-convo-link-container">      
  <div className="pure-g">
    <div className="pure-u-1 center-aligned-text">{this.state.i18n`nav.loadingMessage`}</div>
  </div>
</div>
      ); 
    }

    return (
<div className="match-convo-link-container">      
  <div className="pure-g">
    <div className="pure-u-5-6 right-aligned-text">
      <div className="slim-right-padding slim-vertical-padding">
        <h3 className="match-convo-link-name slim-text no-vertical-margins">{matchedUser.name}</h3>
        <p className="match-convo-link-last-active-blurb no-vertical-margins super-slim-text">
          <i className="fa fa-clock-o"></i>&nbsp;
          {lastMessageSentFragment}
        </p>
      </div>
    </div>

    <div className="pure-u-1-6 center-aligned-text">

      <div className="pure-g">
        <div className="message-count-container match-convo-link-rhs nephritis-bg slim-vertical-padding">
          <div className="pure-u-1 slim-vertical-padding"><i className="fa fa-comment"></i></div>
        </div>
      </div>
    </div>

  </div>
</div>
    );
  }
}

export default MatchConvoLink;

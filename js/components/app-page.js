import React from "react";
import { Link, PropTypes } from "react-router";
import FBAuthStore from "../stores/fb-auth";
import LocaleStore from "../stores/locale";
import ErrorStore from "../stores/error";
import TinderLocationStore from "../stores/tinder-location";
import TinderRecommendationsStore from "../stores/tinder-recommendation";
import TinderSentimentsStore from "../stores/tinder-sentiment";
import Actions from "../actions";
import Constants from "../constants";
import _ from "lodash";

class AppPage extends React.Component {
  constructor(props) {
    super(props);
    this.unsubFunctions = {};
    this.state = {i18n: LocaleStore.state.i18nTemplateFn};
  }

  componentDidMount() {
    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      console.log("[APP PAGE COMPONENT] Received locale update!", locale);
      this.setSTate({i18n: locale.i18nTemplateFn});
    });

    this.unsubFunctions["store-auth"] = FBAuthStore.listen(auth => {
      // Redirect to login if logout happens
      if (!auth.loggedIn) {
        console.log("[APP PAGE COMPONENT] Detected that user has been logged out, redirecting to login page");
        this.context.history.pushState(null, '/login');
      }
    });

    this.unsubFunctions["error"] = ErrorStore.listen(err => {
      // Redirect to error if an error occurs
      this.context.history.pushState(null, '/error');
    });

    this.unsubFunctions["store-user"] = FBAuthStore.listen(auth => {
      console.log("[APP PAGE COMPONENT] Received auth info!", auth);
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions, f => f());
  }

  doLogout() {
    console.log("[APP PAGE COMPONENT] Logging out of app");
    Actions.logout();
  }

  viewMatches() { this.context.history.pushState(null, '/matches'); }
  viewRecs() { this.context.history.pushState(null, '/recs'); }
  goToPreferencesPage() { this.context.history.pushState(null, '/preferences'); }

  render() {
    let appBgClass = Constants.APP_BACKGROUND_CLASS;

    return (
<div className={`app-page-container full-height ${appBgClass}`}>
  <h1 className="no-vertical-margins slim-text picture-overlay-text">{this.state.i18n`app.name`}</h1>
  <h4 className="no-vertical-margins slim-text picture-overlay-text">{this.state.i18n`app.subtitle`}</h4>
  <h1 className="no-bottom-margin slim-text picture-overlay-text">{this.state.i18n`app.tagline.first`}</h1>
  <h1 className="no-vertical-margins slim-text picture-overlay-text alizarin-face">{this.state.i18n`app.tagline.last`}</h1>

  <div className="pinned-to-bottom app-page-controls">

    <div className="row landing-wide-button">
      <button id="recs-page-link-button"
              onClick={this.viewRecs.bind(this)}
              className="pure-u-1 pure-button button-lg squared button-success">
        <i className="fa fa-binoculars"></i> {this.state.i18n`nav.sections.findAMatch`}
      </button>
    </div>

    <div className="row landing-wide-button">
      <button id="matches-page-link-button"
              onClick={this.viewMatches.bind(this)}
              className="pure-u-1 pure-button button-lg squared button-error">
        <i className="fa fa-heart"></i> {this.state.i18n`nav.sections.matches`}
      </button>
    </div>

    <div className="row landing-wide-button">
      <button id="preferences-page-link-button"
              onClick={this.goToPreferencesPage.bind(this)}
              className="pure-u-1 pure-button button-lg squared button-secondary wet-asphalt-bg">
        <i className="fa fa-wrench"></i> {this.state.i18n`nav.sections.preferences`}
      </button>
    </div>

    <div className="row landing-wide-button">
      <button id="logout-button"
              onClick={this.doLogout.bind(this)}
              className="pure-u-1 pure-button button-lg squared">
        <i className="fa fa-sign-out"></i> {this.state.i18n`nav.sections.logout`}
      </button>
    </div>
  </div>
</div>
    );
  }
}

// Add mixins
AppPage.contextTypes = {
  history: PropTypes.history
};

export default AppPage;

import React from "react";
import { toggleStateBoolean } from "../util";

class CollapsibleLabledFormSection extends React.Component {
  constructor(props) {
    super(props);

    this.state = {collapsed: props.collapsed || false};
  }
  render() {
    return (
<section className="collapsible-labeled-form-section">
  <label htmlFor={this.props.labelFor}>
  <i className={`float-right slim-margin-right fa ${this.state.collapsed ? "fa-chevron-down" : "fa-chevron-up"}`}
     onClick={toggleStateBoolean("collapsed", this)}></i>

    {this.props.labelText}
  </label>

  {this.state.collapsed ? '' : this.props.children}
</section>
    );
  }
}

export default CollapsibleLabledFormSection;

import React from "react";
import { PropTypes } from "react-router";
import FBAuthStore from "../stores/fb-auth";
import LocaleStore from "../stores/locale";
import LocaleSelectBox from "./locale-select-box";
import Actions from "../actions";
import Constants from "../constants";
import { parseLocationHashForLoginResp } from "../util";
import _ from "lodash";

class LoginPage extends React.Component {
  constructor(props, context) {
    super(props);
    this.unsubFunctions = {};
    this.state = { i18n: LocaleStore.state.i18nTemplateFn };
    console.log("[LOGIN PAGE COMPONENT] Initializing LoginPage, initial state:");

    // Detect if the page was loaded as a result of a FB login
    let result = parseLocationHashForLoginResp(location.hash);
    if (!_.isNull(result) && _.has(result, 'accessToken')) { Actions.loggedIn(result); }
  }

  componentDidMount() {
    // Detect if the user is already logged in and redirect away
    if (FBAuthStore.state.loggedIn) {
      console.log("[LOGIN PAGE COMPONENT] Detected that user has been logged in, proceeding to app");
      this.context.history.pushState(null, '/app');
      return;
    }

    this.unsubFunctions["locale"] = LocaleStore.listen(locale => {
      this.setState({i18n: locale.i18nTemplateFn});
    });

    this.unsubFunctions["fb-auth-store"] = FBAuthStore.listen(auth => {
      // Check if the user has been logged in and redirect
      // This code might be triggered in the case where the user has already logged in once
      // (so value would be read from localstorage), rather than causing redirect for authentication
      if (auth.loggedIn) {
        console.log("[LOGIN PAGE COMPONENT] Detected that user has been logged in, proceeding to app");
        this.context.history.pushState(null, '/app');
      }
    });
  }

  componentWillUnmount() {
    _.each(this.unsubFunctions);
  }

  doLogin() {
    console.log("[LOGIN PAGE COMPONENT] Performing login action");
    Actions.login();
  }

  render() {
    let appBgClass = Constants.APP_BACKGROUND_CLASS;

    return (
<div className={`full-height ${appBgClass}`}>
  <div className="pure-g">
    <div className="pure-u-1">
      <h1 className="slim-text no-top-margin">{this.state.i18n`app.name`}</h1>
      <h2 className="slim-text">{this.state.i18n`app.subtitle`}</h2>
    </div>

    <div className="pure-u-1">
      <div className="pure-u-1-5"></div>
      <button id="login-button" 
              className="pure-button pure-u-3-5 button-fb" 
              onClick={this.doLogin.bind(this)}>
        <i className="fa fa-facebook-square"></i> {this.state.i18n`pages.login.login`}
      </button>
      <div className="pure-u-1-5"></div>
    </div>

    <div className="pure-u-1">
      <div className="pure-u-1-5"></div>
      <div className="pure-u-3-5">
        <div className="half-opacity-black-bg slim-pad med-margin-top">
          <LocaleSelectBox/>
        </div>
      </div>
      <div className="pure-u-1-5"></div>
    </div>

  </div>
</div>
    );
  }

}

// Add mixins
LoginPage.contextTypes = {
  history: PropTypes.history
};

export default LoginPage;

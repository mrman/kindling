import React from "react";

class TinderGenderIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {gender: props.gender || 0};
  }

  render() {
    let genderIconClass = this.state.gender == 0 ? 'fa-male' : 'fa-female';
    return (<i className={`gender-icon fa ${genderIconClass}`}></i>);
  }
}

export default TinderGenderIcon;

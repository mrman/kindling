import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import TinderAuthStore from "./tinder-auth";
import TinderLocationStore from "./tinder-location";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {
  recs: null,
  currentRec: null,
  location: null,
  tinderAccessToken: null,
  requestInFlight: false,
  recsFetchFailed: false
};

const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_RECOMMENDATION_STORE_STATE);

let RecommendationStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER RECOMMENDATION STORE] Initializing tinder recommendations store...");

    // Setup action listeners
    this.listenTo(Actions.pullTinderRecommendations, this.pullRecommendations);
    this.listenTo(Actions.getNextTinderRecommendation, this.getNextRecommendation);
    this.listenTo(Actions.sentimentSuccessfullyProcessed, this.handleSentimentProcessing);
    this.listenTo(Actions.getCurrentRec, () => this.trigger(this.state));

    // Save the Tinder access token when provided by the TinderAuthStore
    TinderAuthStore.listen(auth => {
      console.log("[TINDER RECOMMENDATION STORE] Detected Tinder auth update:", auth);
      this.state.tinderAccessToken = auth.token;
      this.backupState();

      // If no recs were available yet, request some
      if (_.isEmpty(this.state.recs)) { this.pullRecommendations(); }

      this.updateCurrentRec(); // Update the current rec if not present already
    });

    // Ensure that pos has been set/provided by TinderLocationStore
    TinderLocationStore.listen(location => {
      console.log("[TINDER RECOMMENDATION STORE] Detected location update:", location);
      this.state.location = location;
      this.backupState();
      // If no recs were available yet, request some
      if (_.isEmpty(this.state.recs)) { this.pullRecommendations(); }

      this.updateCurrentRec(); // Update the current rec if not present already
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || _.get(TinderAuthStore, "state.token", null);
    this.state.location = this.state.location || _.get(TinderLocationStore, "state.pos", null);

    // Reset state.recsFetchFailed if recs are present and the value was loaded in from previous state
    // The app might have crashed with recs failing, would like to retry if user closes and reopens the app
    if (!_.isEmpty(this.state.recs) && this.state.recsFetchFailed) { this.state.recsFetchFailed = true; }

    // Attempt to fetch recommendations if access token is present, and a location has already been set with the TinderLocationService
    if (!_.isNull(this.state.tinderAccessToken) &&
        !_.isNull(this.state.location) &&
        _.isEmpty(this.state.recs)) {
      console.log("[TINDER RECOMMENDATION STORE] Detected valid tinder access token and location but no recs, retrieving recs...");
      this.pullRecommendations();
    }

    // Ensure the current rec is the first in list
    this.ensureCurrentRecIsFirstInList();

    console.log("[TINDER RECOMMENDATION STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return _.every([this.state.tinderAccessToken, this.state.location],
                   o => !_.isEmpty(o));
  },

  // Ensure that the current match is the first element of the list, if not, rotate the list
  ensureCurrentRecIsFirstInList() {
    if (!_.eq(this.currentRec, _.first(this.state.recs))) {
      console.log("[TINDER RECOMMENDATION STORE] Inconsistent state, this.state.currentRec is not first item in this.state.recs");

      // Attempt to recover from this invalid state by moving current rec back to first in list
      this.state.currentRec = _.first(this.state.recs);
      this.backupState();
      this.trigger(this.state);
    }
  },

  updateCurrentRec(force=false) {
    // If force hasn't been specified, and currentRec has something in it, don't update
    if (!force && !_.isEmpty(this.state.currentRec)) { return; }

    // Exit early if there are no recs
    if (_.isEmpty(this.state.recs)) { return; }

    // Cycle to the next rec
    let next = this.state.recs.length >= 2 ? this.state.recs[1] : null;
    let rest = this.state.recs.length >= 2 ? this.state.recs.slice(1) : [];

    this.state.recs = rest || [];
    this.state.currentRec = next || null;
    this.backupState();
    this.trigger(this.state);

    console.log("[TINDER RECOMMENDATION STORE] Updated current rec, state:", this.state);
  },

  // Pull recommendations from tinder API
  pullRecommendations() {
    console.log("[TINDER RECOMMENDATION STORE] Attempting to pull recommendations from tinder...");

    // Exit early if not all necessary credentials are available yet
    if (!this.hasNecessaryCredentials()) {
      console.log("[TINDER RECOMMENDATION STORE] Missing necessary credentials for fetching recs from tinder...");
      return;
    }

    // Exit early if there's a request in flight already
    if (this.state.requestInFlight) {
      console.log("[TINDER RECOMMENDATION STORE] Request already in flight, skipping fetch...");
      return;
    }

    // Fetch Tinder recs
    console.log("[TINDER RECOMMENDATION STORE] Performing GET...");
    this.state.requestInFlight = true;
    TinderAPI.GETWithToken(Constants.TINDER_RECS_URL, this.state.tinderAccessToken)
      .then(resp => resp.json())
      .then(resp => {
        this.state.requestInFlight = false;
        if (resp.status == 200) {
          // Recs came through
          console.log("[TINDER RECOMMENDATION STORE] Successfully fetched recs: ", resp.results);
          this.state.recs = _.isNull(this.state.recs) ? resp.results : this.state.recs.concat(resp.results);
          this.state.currentRec = _.isNull(this.state.recs) ? null : _.first(this.state.recs);

          this.state.recsFetchFailed = false;
          this.backupState();
          this.trigger(this.state);
        } else if (resp.status === "500") {
          // Request went through but tinder reported error
          console.log("[TINDER RECOMMENDATION STORE] Failed to retrieve recs, possible limit hit: ", resp);
          this.state.recsFetchFailed = false;
          this.backupState();
          this.trigger(this.state);
        } else {
          // Not sure what happened here
          console.log("[TINDER RECOMMENDATION STORE] Failed to retrieve recs, response: ", resp);
          this.state.recsFetchFailed = true;
          this.backupState();
          this.trigger(this.state);
        }
      })
      .catch(err => {
        this.state.requestInFlight = false;
        this.state.recsFetchFailed = true;
        this.backupState();
        this.trigger(this.state);
        alert("Failed to fetch Tinder recommendations");
        console.log("[TINDER RECOMMENDATION STORE] Failed to fetch recs: ", err);
      });
  },

  // Pull recommendations from tinder API
  getNextRecommendation() {
    // Quit early if there are no recommendations
    // TODO: Attempt to fetch new recommendations automatically if it runs out?
    if (_.isEmpty(this.state.recs)) {
      console.log("[TINDER RECOMMENDATION STORE] Attempted to get next recommendation, but no more recommendations available");
      return;
    }

    this.updateCurrentRec(true);
  },

  handleSentimentProcessing(s) {
    console.log("[TINDER RECOMMENDATION STORE] Detected successful sentiment processing", s);

    // Get the next recommendation if the sentiment was processed
    // If state.currentRec is null, this must have been the last sentiment (for current batch of recs)
    let currentRecMatches = this.state.currentRec ? (s.userId === this.state.currentRec._id) : false;
    if (currentRecMatches) {
      this.getNextRecommendation();
    } else {
      // If the current rec wasn't the right user, ensure that it's removed from the list of recs
      this.state.recs = _.filter(this.state.recs, r => r._id !== s.userId);
      this.ensureCurrentMatchIsInList(); // In case we get out of sync somehow
      this.backupSate();
      this.trigger(this.state);
    }
  }

});

export default RecommendationStore;

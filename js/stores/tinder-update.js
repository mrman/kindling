import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import TinderAuthStore from "./tinder-auth";
import TinderUserStore from "./tinder-user";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {lastUpdate: null, currentUser: null, tinderAccessToken: null, lastUpdateTime: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_UPDATE_STORE_STATE);

let TinderUpdateStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER UPDATE STORE] Initializing tinder sentiments store...");

    // Setup action listeners
    this.listenTo(Actions.retrieveTinderUpdates, this.retrieveTinderUpdates);

    // Save the Tinder access token when provided by the TinderAuthStore
    TinderAuthStore.listen(auth => {
      console.log("[TINDER UPDATE STORE] Detected Tinder auth update:", auth);
      this.state.tinderAccessToken = auth.token;
      this.backupState();
    });

    // Save the Tinder user when provided by the TinderUserStore
    TinderUserStore.listen(store => {
      console.log("[TINDER UPDATE STORE] Detected tinder user store update", store);
      this.state.currentUser = store.user;
      this.backupState();
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || TinderAuthStore.state.token;
    this.state.currentUser = this.state.currentUser || TinderUserStore.state.user;

    console.log("[TINDER UPDATE STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return _.every([this.state.tinderAccessToken],
                   o => !_.isEmpty(o));
  },

  // Retrieve updates from the Tinder API
  retrieveTinderUpdates() {
    // Exit early if not all necessary credentials are available yet
    if (!this.hasNecessaryCredentials()) {
      reject(new Error("Missing credentials"));
      // If the user was unable to receive updates from Tinder, might as well just log them out
      Actions.logout();
      return;
    }

    // Use the last update time to restrict updates to be new ones only
    let lastUpdateTime = _.isNull(this.state.lastUpdateTime) ? "" : this.state.lastUpdateTime;

    console.log(`[TINDER UPDATE STORE] Retrieving updates from Tinder API (lastUpdateTime = ${lastUpdateTime})...`);
    TinderAPI.POSTWithToken(Constants.TINDER_UPDATES_URL,
                            {last_activity_date: lastUpdateTime},
                            this.state.tinderAccessToken)
      .then(resp => {
        console.log("[TINDER UPDATE STORE] Raw return from update request:", resp);
        return resp.json();
      })
      .then(resp => {
        console.log("[TINDER UPDATE STORE] Received updates from tinder:", resp);

        this.state.lastUpdate = resp;
        this.state.lastUpdateTime = new Date();

        // Update state/trigger actions
        this.backupState();
        this.trigger(this.state);
      });
  }

});

export default TinderUpdateStore;

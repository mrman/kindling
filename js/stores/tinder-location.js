/** global navigator */
/** global localStorage */

import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import TinderAuthStore from "./tinder-auth";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {pos: null, tinderAccessToken: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_LOCATION_STORE_STATE);

let TinderLocationStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER LOCATION STORE] Initializing tinder location store...");

    // Setup action listeners
    this.listenTo(Actions.updateLocationFromGPS, _.debounce(this.updateLocation, 500));

    // Save the Tinder access token
    TinderAuthStore.listen(auth => {
      this.state.tinderAccessToken = auth.token;
      this.backupState();
      this.updateLocation();
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || _.get(TinderAuthStore, "state.token", null);

    // Attempt to update location upon start of location service
    this.updateLocation();

    console.log("[TINDER LOCATION STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return !_.isEmpty(this.state.tinderAccessToken);
  },

  getLocation(forceUpdate=false) {
    return new Promise((resolve, reject) => {

      // If this.state.pos is not undefined/null/empty, and an update is not being forced, immediately return it
      if (!forceUpdate && !_.isEmpty(this.state.pos)) {
        resolve(this.state.pos);
        return;
      }

      // Error and exit early if geolocation is not available
      if (!('geolocation' in navigator)) {
        let err = {
          type: "error",
          msg: "Geolocation is disabled. Please enable geolocation for the app to fetch your location."
        };
        Actions.error(err);
        reject(new Error(err.msg));
        return;
      }

      // Retrieve longitude/latitude from FFOS geolocation API
      navigator.geolocation
        .getCurrentPosition(pos => {
          console.log("[TINDER LOCATION STORE] Retrieved location from Geolocation API:", pos);
          this.state.pos = pos;
          this.backupState();
          resolve(this.state.pos);
        }, reject);
    });
  },

  // Pull location from tinder API
  updateLocation(forceUpdate=false) {
    // Exit early if not all necessary credentials are available yet
    if (!this.hasNecessaryCredentials()) { return; }
    console.log("[TINDER LOCATION STORE] Updating location with Tinder API...");

    // Get location
    this.getLocation(forceUpdate)
      .then(pos => {
        // Update loscation with tinder
        TinderAPI.POSTWithToken(Constants.TINDER_LOCATION_URL,
                                {lat: pos.coords.latitude, lon: pos.coords.longitude},
                                this.state.tinderAccessToken)
          .then(resp => resp.json())
          .then(resp => {
            if (resp.status == 200) {
              console.log("[TINDER LOCATION STORE] Successfully updated location, position: ", pos);
              this.trigger(this.state);
            } else {
              console.log("[TINDER LOCATION STORE] Failed to update location, response: ", resp);
            }
          })
          .catch(err => {
            alert("Failed to update your tinder location... Please try again later");
            console.log("[TINDER LOCATION STORE] Failed to update tinder location: ", err);
          });
      })
      .catch(err => {
        alert("Failed to retrieve location from GPS, please turn on location services, and restart Kindling.");
        console.log("[TINDER LOCATION STORE] Failed to retrieve location from GPS: ", err);
      });
  }
});

export default TinderLocationStore;

import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import FBUserStore from "./fb-user";
import FBAuthStore from "./fb-auth";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";
import { jsonPOSTRequest } from "../util";

const DEFAULT_STATE = {auth: null, token: null, fbUser: null, fbAccessToken: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_AUTH_STORE_STATE);

let TinderAuthStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER AUTH STORE] Initializing tinder auth store...");

    // Register actions
    this.listenTo(Actions.authenticateWithTinder, function() {
      this.getTinderAccessToken();
    });

    // Save the facebook user id of the current user
    FBUserStore.listen(user => {
      console.log("[TINDER AUTH STORE] Detected FBUser store update, saving facebook user info...");
      this.state.fbUser = user;
      this.getTinderAccessToken();
    });

    // Save the fbToken of the current user
    FBAuthStore.listen(auth => {
      console.log("[TINDER AUTH STORE] Detected FBAuthStore store update, saving facebook access token...");
      this.state.fbAccessToken = auth.token;
      this.getTinderAccessToken();
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.fbAccessToken = this.state.fbAccessToken || _.get(FBAuthStore, "state.token", null);
    this.state.fbUser = this.state.fbUser || _.get(FBUserStore, "state.user", null);

    // Retrieve access token from tinder if fbAccessToken and user were loaded but token was not
    if (!_.isNull(this.state.fbUser) && !_.isNull(this.state.fbAccessToken) && _.isNull(this.state.token)) {
      console.log("[TINDER AUTH STORE] Detected non-null user, fbAccessToken, requesting tinder access token");
      this.getTinderAccessToken();
    }

    console.log("[TINDER AUTH STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return _.every([this.state.fbUser, this.state.fbAccessToken],
                   o => !_.isEmpty(o));
  },

  // Triggered when the user clicks a login button (generally on LoginPage)
  getTinderAccessToken() {
    if (!this.hasNecessaryCredentials()) { return; }
    console.log("[TINDER AUTH STORE] Attempting to obtain a tinder access token");

    // Authenticate with Tinder
    TinderAPI.POST(
      Constants.TINDER_AUTH_URL,
      {
        facebook_token: this.state.fbAccessToken,
        facebook_id: this.state.fbUser.id
      })
      .then(resp => resp.json())
      .then(auth => {
        console.log("[TINDER AUTH STORE] Successfully authenticated with tinder API");
        this.state = _.extend(this.state, {auth: auth, token: auth.token});
        this.backupState();
        this.trigger(this.state);
      })
      .catch(err => {
        alert("Failed to authenticate with Tinder API. Please try again later, or re-install Kindling from the marketplace.");
        console.log("[TINDER AUTH STORE] Failed to authenticate with tinder api:", err);
        this.trigger(this.state);
      });

    }
});

export default TinderAuthStore;

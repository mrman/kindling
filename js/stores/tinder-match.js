import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import TinderUpdateStore from "./tinder-update";
import TinderAuthStore from "./tinder-auth";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {matches: {}, matchedUserInfo: {}, tinderAccessToken: null};
const MATCH_DEFAULT_STATE = {messages: [], pendingMessages: []};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_MATCH_STORE_STATE);

function generateTinderURLForMatchMessageSend(matchId) {
  return Constants.TINDER_MATCH_MESSAGE_URL`${matchId}`;
}
function generateTinderURLForUserInfo(userId) {
  return Constants.TINDER_USER_INFO_URL`${userId}`;
}
function generateTinderURLForDeleteMatch(matchId) {
  return Constants.TINDER_DELETE_MATCH_URL`${matchId}`;
}

let TinderMatchStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER MATCH STORE] Initializing tinder match store...");

    // Subscribe to actions
    this.listenTo(Actions.retrieveMatchedUserInfo, this.retrieveMatchedUserInfo);
    this.listenTo(Actions.sendMessageToMatch, this.sendMessageToMatch);
    this.listenTo(Actions.matchResultDetected, this.matchResultDetected);

    // Save the Tinder access token when provided by the TinderUpdateStore
    TinderUpdateStore.listen(store => {
      console.log("[TINDER MATCH STORE] Detected Tinder update store update fetch:", store);
      this.processTinderUpdate(store.currentUser, store.lastUpdate);
    });

    // Save the Tinder access token when provided by the TinderAuthStore
    TinderAuthStore.listen(auth => {
      console.log("[TINDER UPDATE STORE] Detected Tinder auth update:", auth);
      this.state.tinderAccessToken = auth.token;
      this.backupState();
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || TinderAuthStore.state.token;

    // Attempt to clear out duplicate messages for all matches if present
    _.forEach(this.state.matches, m => {
      m.messages = _.uniqBy(m.messages, _.property('_id'));
    });

    console.log("[TINDER MATCH STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return !_.isEmpty(this.state.tinderAccessToken);
  },

  // Handle a match result detected (usually from a "like" sentiment expression)
  matchResultDetected(user) {
    console.log("[TINDER MATCH STORE] Detected a match from like sentiment expression, user:", user);
    this.addMatchedUser(user);
  },

  /**
   * Add a matched user
   *
   * @param {object} user - the matched user
   */
  addMatchedUser(user) {
    let userId = _.get(user, "_id");

    if (_.isUndefined(userId)) {
      console.log("[TINDER MATCH STORE] Failed to add matched user, invalid user object:", user);
      return;
    }

    this.state.matchedUserInfo[userId] = user;
    this.backupState();
    this.trigger(this.state);
  },

  /**
   * Retrieve info for a matched user
   *
   * @param {string} matchId - Match ID of the matched user
   */
  retrieveMatchedUserInfo(userId) {
    if (_.isNull(userId) || _.isUndefined(userId)) {
      throw new Error("Invalid userId");
    }

    // Attempt to find given matched user's info
    console.log(`[TINDER MATCH STORE] Retrieveing user info for user with ID ${userId}`);
    TinderAPI.GETWithToken(generateTinderURLForUserInfo(userId), this.state.tinderAccessToken)
      .then(resp => resp.json())
      .then(resp => {
        if (resp.status !== 200) {
          console.log(`[TINDER MATCH STORE] Failed to retrieve user data for user with ID ${userId} :`, resp);
          return;
        }

        let user = resp.results;
        this.addMatchedUser(user); // Will trigger as necessary
      })
      .catch(err => {
        console.log("[TINDER MATCH STORE] Error occurred while trying to get matched user's info:", err);
      });
  },

  /**
   * Send a message to a twitter match
   *
   * @param {string} matchId - Tinder match id
   * @param {string} msg - the message to send
   */
  sendMessageToMatch(matchId, message) {
    if (!this.hasNecessaryCredentials()) { throw new Error("Necessary creds missing"); }

    // Throw error in the case of an invalid match ID or message
    if (_.isUndefined(matchId) || _.isNull(matchId) ||
        _.isUndefined(message) || _.isNull(message)) {
      throw new Error(`Invalid parameters - match ID and message must be present`);
    }

    // Trim the message, return if empty
    message = message.trim();
    if (_.isEmpty(message)) {
      console.log("[TINDER MATCH STORE] Ignoring empty message send");
      return;
    }

    // Update the messages for that match preemptively
    this.state.matches[matchId].pendingMessages.push({matchId, message});
    this.backupState();
    this.trigger(this.state);

    // Attempt to send message to match
    console.log("[TINDER MATCH STORE] Attempting to send message to match with id:", matchId);
    TinderAPI.POSTWithToken(generateTinderURLForMatchMessageSend(matchId),
                            {message},
                            this.state.tinderAccessToken)
      .then(resp => resp.json())
      .then(msg => {
        // Move the proof of the pending message to
        let relevantPendingMessage = _.find(this.state.matches[matchId].pendingMessages,
                                    p => p.message === msg.message);

        // Trim pending message array
        let trimmedPendingMessages = _.filter(this.state.matches[matchId].pendingMessages,
                                              p => p.message !== relevantPendingMessage.message);

        // Add the message object as received from tinder to the list of messages, update pending messages
        this.state.matches[matchId].messages.push(msg);
        this.state.matches[matchId].pendingMessages = trimmedPendingMessages;

        // Alert others to the fact that we've received a sent message receipt for this match
        Actions.receivedSentMessageReceipt(matchId, msg);

        // Alert to received sent message receipt
        this.backupState();
        this.trigger(this.state);
      })
      .catch(err => {
        console.log(`[TINDER MATCH STORE] Failed to send message to match with ID ${matchId}:`, err);
      });
  },

  /**
   * Unmatch a user
   *
   * @param {string} id - Match ID of the matched user to be unmatched
   */
  umatchUser(matchId) {
    if (!this.hasNecessaryCredentials()) { throw new Error("Necessary creds missing"); }

    // Attempt to find user Info for given match
    TinderAPI.DELETEWithToken(generateTinderURLForDeleteMatch(matchId), this.state.tinderAccessToken)
      .then(resp => resp.json())
      .then(resp => { Actions.successfullyDeletedMatch(matchId, resp); });
  },

  /**
   * Parse a tinder update for matches and other information relevant to this store
   *
   * @param {object} currentUser - Current kindling user
   * @param {object} update - The update object from tinder
   */
  processTinderUpdate(currentUser, update) {
    console.log("[TINDER MATCH STORE] Parsing received tinder update", update);

    let matches = update.matches || [];

    // Process all matches in the update
    console.log(`[TINDER MATCH STORE] Processing ${matches.length} matches...`);
    _.forEach(matches, (m) => {
      let matchId = m._id;

      // Create match match record if it doesn't exist already
      if (!_.has(this.state.matches, matchId)) {
        this.state.matches[matchId] =  _.extend({}, MATCH_DEFAULT_STATE);
      }

      // Augment and add messages to state
      _.forEach(m.messages, m => {
        // TODO: this is really inefficient... maybe use a set or a map for messages instead??
        let messageAlreadyExists = _.find(this.state.matches[matchId].messages,
                                          _.matchesProperty('id', m.id));

        // Append augmented messages, initialized with false seenByUser flag
        if (!messageAleradyExists) {
          this.state.matches[matchId].messages.push(_.extend(m, {seenByUser: false}));
        }
      });

      // Attempt to discover the correct matchedUserId
      let matchedUserId = "";
      if(!_.isUndefined(m.person)) {
        // If m.person has been provided, use that to find the matched user ID, and use it to update the state
        // Initial match object should contain at least
        matchedUserId = m.person._id;
      } else if(!_.isEmpty(m.messages)) {
        // Attempt to derive matched user ID from a message
        matchedUserId = m.message.to === currentUser._id ? m.message.from : m.message.to;
      }

      // Save matched user ID, initial match object to internal matches
      this.state.matches[matchId].matchedUserId = matchedUserId;
      this.state.matches[matchId].matchObj = m;

      // If there is pre-existing match object, but we're getting piecemeal updates, there's a problem
      if (!_.has(this.state.matches, matchId) && _.isUndefined(m.person)) {
        // TODO: Need to request fresh updates/specific match info in hopes of getting proper data
        console.log("[TIDNER MATCH STORE] WARNING: no preexisting match object, but we're gettin piecemeal updates. Need to request a full updates object");
      }

      // Set match ID and matched user ID
      this.state.matches[matchId].id = matchId;
    });

    // Save state and alert listeners to updates
    this.backupState();
    this.trigger(this.state);
  }

});

export default TinderMatchStore;

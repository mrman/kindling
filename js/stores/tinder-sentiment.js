import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import TinderAuthStore from "./tinder-auth";
import TinderAPI from "../tinder-api";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {lastSentiment: null, pendingSentiments: [], tinderAccessToken: null, outOfLikes: false, outOfLikesUntil: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_SENTIMENT_STORE_STATE);

function sentimentIsValid(sentiment) {
  return  !_.isNull(sentiment) &&
    !_.isEmpty(sentiment.userId) &&
    !_.isEmpty(sentiment.value);
}

function generateTinderURLFromSentiment(sentiment) {
  return Constants.TINDER_SENTIMENT_URL`Express ${sentiment.value.toLowerCase()} for user with ID ${sentiment.userId}`;
}

let TinderSentimentStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER SENTIMENT STORE] Initializing tinder sentiments store...");

    // Setup action listeners
    this.listenTo(Actions.like, this.likeUser);
    this.listenTo(Actions.pass, this.passOnUser);

    // Save the Tinder access token when provided by the TinderAuthStore
    TinderAuthStore.listen(auth => {
      console.log("[TINDER SENTIMENT STORE] Detected Tinder auth update:", auth);
      this.state.tinderAccessToken = auth.token;
      this.backupState();

      this.processPendingSentiments(); // Process sentiments that occurred while no auth was available
    });

    // Gather state for the store from localstorage/other sources
    // Also attempt to pull state from parent stores if they have initialized and have state
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || TinderAuthStore.state.token;

    // Attempt to process pending sentiments if access token is present, and this.state.pendingSentiments is non-empty
    if (!_.isNull(this.state.tinderAccessToken) &&
        !_.isEmpty(this.state.pendingSentiments)) {
      console.log("[TINDER SENTIMENT STORE] Detected valid tinder access token and pending sentiments, processing...");
      this.processPendingSentiments();
    }

    console.log("[TINDER SENTIMENT STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    let hasCreds =  _.every([this.state.tinderAccessToken], o => !_.isEmpty(o));
    let outOfLikes = this.state.outOfLikes && ((new Date().getTime()) < this.state.outOfLikesUntil);
    return hasCreds && !outOfLikes;
  },

  // Callbacks for expressing sentiment for a user
  likeUser(user) {
    this.expressSentiment(user, Constants.SENTIMENT_LIKE)
      .then(likeResult => {

        // If no likes are remaining (no more sentiment can be expressed as a result), alert user
        if ('likes_remaining' in likeResult && likeResult['likes_remaining'] == 0) {
          this.state.outOfLikes = true;
          this.state.outOfLikesUntil = likeResult["rate_limited_until"];
          this.backupState();
          this.trigger(this.state);
        }
      });
  },
  passOnUser(user) { this.expressSentiment(user, Constants.SENTIMENT_PASS); },

  // Pull sentiments from tinder API
  processPendingSentiments() {
    console.log(`[TINDER SENTIMENT STORE] Attempting to process ${this.state.pendingSentiments.length} pending sentiments...`);

    // Exit early if not all necessary credentials are available yet
    if (!this.hasNecessaryCredentials() || _.isEmpty(this.state.pendingSentiments)) { return; }

    // Process all pending sentiments
    _.forEach(this.state.pendingSentiments, this.processSentiment);
  },

  /**
   * Process a single sentiment (like/dislike of another tinder user)
   *
   * @param {object} sentiment - An object containing the user and the sentiment expressed
   * @param {string} sentiment.userId - An tinder ID of the user
   * @param {string} sentiment.value - Constants representing a tinder LIKE or PASS
   */
  processSentiment(sentiment) {

    return new Promise((resolve, reject) => {

    // Exit early if not all necessary credentials are available yet
    if (!this.hasNecessaryCredentials() || !sentimentIsValid(sentiment)) {
      reject(new Error("Missing credentials or invalid sentiment"));

      // Send out current state
      this.trigger(this.state);

      // Logout if the user doesn't have necessary credentials
      if (!this.hasNecessaryCredentials()) {
        Actions.logout();
      }

      return;
    }

      console.log("[TINDER SENTIMENT STORE] URL for sentiment:", generateTinderURLFromSentiment(sentiment));
      TinderAPI.GETWithToken(generateTinderURLFromSentiment(sentiment), this.state.tinderAccessToken)
        .then(resp => resp.json())
        .then(resp => {
          // Handle sentiment failure (for whatever reason tinder specifies)
          if (_.has(resp, "status") && resp.status !== 200) {
            reject(new Error("An error occurred while trying to process sentiment, response:", resp));
          }

          // Handle a possible match
          if (_.has(resp, "match") && resp.match) {
            console.log(`[TINDER SENTIMENT STORE] Detected match result with user ${sentiment.userId}:`, resp.match);
            Actions.matchResultDetected(sentiment, resp.match);
          }

          // Update state/trigger actions
          this.state.lastProcessedSentiment = sentiment;
          Actions.sentimentSuccessfullyProcessed(sentiment);
          resolve(resp);
          this.backupState();
          this.trigger(this.state);
        })
        .catch(reject);
    });
  },

  /**
   * Express sentiment for a given user to the Tinder API
   *
   * @param {string} user - The user object of the user for which to express sentiment
   * @param {string} value - a constant representing which sentiment to express
   */
  expressSentiment(user, value) {
    let sentiment = {userId: user._id, value: value};

    return new Promise((resolve, reject) => {
      // Quit early for invalid sentiments
      if (!sentimentIsValid(sentiment)) {
        console.log("[TINDER SENTIMENT STORE] Detected invalid sentiment:", sentiment);
        reject(new Error("Invalid sentiment:", sentiment));
        return;
      }

      // Process the sentiment
      this.processSentiment(sentiment)
        .then(resolve)
        .catch(err => {
          alert("Failed to save like/dislike to Tinder.");
          Actions.logout(); // Logout if there was a failure to process
          // If processing a sentiment fails, save it in the pending list
          this.state.pendingSentiments.push(sentiment);
          reject(err);
        });
    });
  }

});

export default TinderSentimentStore;

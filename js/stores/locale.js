/** global localStorage */
/** global location */

import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import { SUPPORTED_LOCALES, isValidLocaleCode, getLocaleData, generateI18NTemplateFn } from "../i18n";
import _ from "lodash";

const DEFAULT_LOCALE_DATA = getLocaleData();
const DEFAULT_LOCALE_CODE = DEFAULT_LOCALE_DATA.code;
const DEFAULT_STATE = {
  locale: DEFAULT_LOCALE_DATA, // Will return default locale
  defaultLocale: DEFAULT_LOCALE_DATA,
  supportedLocales: SUPPORTED_LOCALES
};

const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_LOCALE_STORE_STATE);

let LocaleStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    // Subscribe to actions
    this.listenTo(Actions.changeLocaleByCode, this.changeLocaleByCode);

    // Gather state for the store from localstorage/other sources
    this.state = _.extend(DEFAULT_STATE, this.gatherState());

    // Set supported locales (cache might have an older list of supported locales
    this.state.supportedLocales = SUPPORTED_LOCALES;

    // Generate locale templating function
    this.state.i18nTemplateFn = generateI18NTemplateFn(this.state.locale.code);

    console.log("[LOCALE STORE] Initial state:", _.extend({}, this.state));
  },

  changeLocaleByCode(code) {
    if (!isValidLocaleCode(code)) {
      console.log(`[LOCALE STORE] Invalid Locale [${code}]`);
      return;
    }

    // Update local and template function
    console.log(`[LOCALE STORE] Changing locale to [${code}]`);
    this.state.locale = getLocaleData(code);
    this.state.i18nTemplateFn = generateI18NTemplateFn(this.state.locale.code);
    this.backupState();
    this.trigger(this.state);
  }

});

export default LocaleStore;

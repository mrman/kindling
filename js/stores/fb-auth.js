/** global localStorage */
/** global location */

import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {loggedIn: false, token: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_FB_AUTH_STORE_STATE);

let FBAuthStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[FB AUTH STORE] Initializing fb auth store...");

    // Subscribe to actions
    this.listenTo(Actions.login, this.onLogin);
    this.listenTo(Actions.loggedIn, this.onLoggedIn);
    this.listenTo(Actions.logout, this.onLogout);

    // Gather state for the store from localstorage/other sources
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    console.log("[FB AUTH STORE] Initial state:", _.extend({}, this.state));
  },

  // Triggered when the user clicks a login button (generally on LoginPage)
  onLogin() {
    console.log(`[FB AUTH STORE] Attempting to login, current token: ${this.token}`);

    if (_.isUndefined(this.state.token) || _.isNull(this.state.token)) {
      // Redirect if token is null (user has not logged in with FB yet logged in)
      location.href = Constants.FB_AUTH_TOKEN_HREF;
      return;
    }

    console.log(`[FB AUTH STORE] Detected user is already logged in with token: [${this.state.token}]`);
    this.state = _.extend(this.state, {loggedIn: true, token: this.state.token});
    this.backupState();
    this.trigger(this.state);
  },

  // Triggered when some component (usually App) notices that a login has occurred, using location.hash
  onLoggedIn(info) {
    console.log(`[FB AUTH STORE] Detected login redirect, current token: ${this.state.token}`);

    // Do nothing if we were not properly logged in
    if (!_.has(info, 'accessToken') || _.isEmpty(info.accessToken)) {
      console.log("[FB AUTH STORE] Invalid/Incomplete info object, user is not logged in! info:", info);
      return;
    }

    // Set state, backup, and Save and set token
    console.log(`[FB AUTH STORE] Access token set to [${info.accessToken}]`);
    this.state = _.extend(this.state, {
      loggedIn: true,
      token: info.accessToken,
      tokenReceivedTime: new Date(),
      tokenExpiresIn: info.expiresIn
    });
    this.backupState();
    this.trigger(this.state);
  },

  // Perform logout
  onLogout() {
    console.log("[FB AUTH STORE] Performing logout");
    this.state = _.extend(this.state, {
      loggedIn: false,
      token: null,
      tokenReceivedTime: null,
      tokenExpiresIn: null
    });
    this.backupState();
    this.trigger(this.state);

    // Remove from localstorage
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_FB_AUTH_STORE_STATE);
  }

});

export default FBAuthStore;

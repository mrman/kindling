import Reflux from "reflux";
import Actions from "../actions";
import Constants from "../constants";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import _ from "lodash";

const DEFAULT_STATE = {errors: []};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_ERROR_STORE_STATE);

let ErrorStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    this.state = DEFAULT_STATE;
    this.state = _.extend(this.state, this.gatherState());

    // Setup actions
    this.listenTo(Actions.error, (err) => {
      this.state.errors.push(err);
      this.backupState();
      this.trigger(this.state);
    });

    console.log("[ERROR STORE] Initial state:", this.state);
  }
});

export default ErrorStore;

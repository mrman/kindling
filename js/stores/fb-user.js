/* global localStorage */

import Reflux from "reflux";
import Constants from "../constants";
import Actions from "../actions";
import FBAuthStore from "./fb-auth";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import { getGlobalFetch, generateURLWithQueryParams } from "../util";
import _ from "lodash";

const DEFAULT_STATE = {user: null, fbAccessToken: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_FB_USER_STORE_STATE);

let FBUserStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[FB USER STORE] Initializing user store...");

    // Subscribe to actions
    this.listenTo(Actions.logout, this.onLogout);

    // Listen to Auth store for user logins
    FBAuthStore.listen(auth => {
      if (auth.loggedIn) {
        console.log("[FB USER STORE] Just detected user login!");
        this.onFBTokenChange.call(this, auth.token);
      }
    });

    // Gather state for the store from localstorage/other sources
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.fbAccessToken = this.state.fbAccessToken || _.get(FBAuthStore, "state.token", null);

    // If state wasn't restored (but, user FB token to retrieve it
    let fbAccessTokenIsValid = !_.isUndefined(this.state.fbAccessToken) && !_.isNull(this.state.fbAccessToken);
    let userIsInvalid = _.isUndefined(this.state.user) || _.isNull(this.state.user);
    if (fbAccessTokenIsValid && userIsInvalid) {
      console.log("[FB USER STORE] Retrieving user info due to incomplete restore from localstorage!");
      this.retrieveUserInfo()
        .then(user => {
          this.state = _.extend(this.state, {user: user});
          this.backupState();
          this.trigger(this.state);
        });
    }

    console.log("[FB USER STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() { return !_.isEmpty(this.state.fbAccessToken);  },

  // Retreive user information using access token
  retrieveUserInfo(token) {
    if (!this.hasNecessaryCredentials()) { 
      return Promise.reject(new Error("Necessary credentials missing:"));
    }

    // Generate the URL
    let userInfoURL = generateURLWithQueryParams(Constants.FB_CURRENT_USER_URL,
                                                 {'access_token': this.state.fbAccessToken});

    // When a user logs in, retrieve their information
    return getGlobalFetch()(userInfoURL)
      .then(d => d.json());
  },

  onFBTokenChange(token) {
    console.log("[FB USER STORE] Detected that a user has logged in!");

    this.state = _.extend(this.state, {fbAccessToken: token});

    this.retrieveUserInfo()
      .then(user => {
        console.log("[FB USER STORE] Retrieved user data:", user);

        this.state = _.extend(this.state, {user: user});
        this.backupState();
        this.trigger(this.state);
      });
  },

  onLogout() {
    console.log("[FB USER STORE] Performing logout");

    // Update state
    this.state = DEFAULT_STATE;
    this.trigger(this.state);

    // Remove from localstorage
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_USER_STORE_STATE);
  }
});

export default FBUserStore;

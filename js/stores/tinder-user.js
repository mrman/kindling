/* global localStorage */

import Reflux from "reflux";
import Constants from "../constants";
import Actions from "../actions";
import TinderAuthStore from "./tinder-auth";
import LocalStorageBackupMixinGenerator from "../mixins/localstorage-backup";
import TinderAPI from "../tinder-api";
import { isValidTinderUserPreferencesObject } from "../util";
import _ from "lodash";

const DEFAULT_STATE = {user: null, tinderAccessToken: null};
const LocalStorageBackupMixin = LocalStorageBackupMixinGenerator(Constants.LOCALSTORAGE_KEY_TINDER_USER_STORE_STATE);

let TinderUserStore = Reflux.createStore({
  mixins: [LocalStorageBackupMixin],
  init() {
    console.log("[TINDER USER STORE] Initializing tinder user store...");

    // Subscribe to actions
    this.listenTo(Actions.logout, this.onLogout);
    this.listenTo(Actions.updateTinderPreferences, this.updateTinderPreferences);
    this.listenTo(Actions.republishTinderUser, () => {
      console.log("[TINDER USER STORE] Republishing tinder user");
      this.trigger(this.state);
    });

    // Listen to Auth store for user logins
    TinderAuthStore.listen(this.onTinderAuthChange);

    // Gather state for the store from localstorage/other sources
    this.state = _.extend(DEFAULT_STATE, this.gatherState());
    this.state.tinderAccessToken = this.state.tinderAccessToken || _.get(TinderAuthStore, "state.token", null);
    this.state.user = this.state.user || _.get(TinderAuthStore, "state.auth.user", null);

    console.log("[TINDER USER STORE] Initial state:", _.extend({}, this.state));
  },

  // Check whether the necessary credentials have been provided
  hasNecessaryCredentials() {
    return !_.isEmpty(this.state.tinderAccessToken) && !_.isEmpty(this.state.user);
  },

  onTinderAuthChange(auth) {
    console.log("[TINDER USER STORE] Just detected tinder auth information update!");

    this.state = _.extend(this.state, {
      tinderAccessToken: auth.token || this.state.tinderAccessToken,
      user: auth.user || this.state.user
    });
    this.backupState();
    this.trigger(this.state);
  },

  /**
   * Update Tinder search preferences
   *
   * @param {Object} prefs - An object with relevant Tinder preferences
   * @param {number} prefs.gender - The current user's gender
   * @param {number} prefs.age_filter_min - Minimum age
   * @param {number} prefs.age_filter_max - Maximum age
   * @param {number} prefs.distance_filter - Maximum distance from user
   */
  updateTinderPreferences(prefs) {
    console.log("[TINDER USER STORE] Attempting to update tinder preferences:", prefs);

    // Quit early if prefs object is invalid
      if (!isValidTinderUserPreferencesObject(prefs)) {
        alert("Preferences invalid, please ensure all fields are properly filled out.");
        console.log("[TINDER USER STORE] Invalid user preferences object:", prefs);
        return;
      }

      TinderAPI.POSTWithToken(Constants.TINDER_PROFILE_URL, prefs, this.state.tinderAccessToken)
        .then(resp => resp.json())
        .then(user => {
          console.log("[TINDER USER STORE] Received response (user object):", user);

          // Update saved user data with user data received from tinder api
          console.log("[TINDER USER STORE] Successfully updated user preferences!", prefs);
          this.state.user = _.extend(this.state.user, user);
          this.backupState();
          this.trigger(this.state);

          alert("Successfully updated Tinder preferences");
        })
        .catch(err => {
          alert("Failed to update tinder preferences.");
          console.log("[TINDER USER STORE] Error updating tinder user preferences:", err);
        });
  },

  onLogout() {
    console.log("[TINDER USER STORE] Performing logout");

    // Update state
    this.state = DEFAULT_STATE;
    this.backupState();
    this.trigger(this.state);

    // Remove from localstorage
    localStorage.removeItem(Constants.LOCALSTORAGE_KEY_TINDER_USER_STORE_STATE);
  }
});

export default TinderUserStore;

// App-related
let LOCALSTORAGE_KEY_ERROR_STORE_STATE = "store-error-state";
let LOCALSTORAGE_KEY_LOCALE_STORE_STATE = "store-locale-state";
let LOCALSTORAGE_KEY_FB_AUTH_STORE_STATE = "store-fb-auth-state";
let LOCALSTORAGE_KEY_FB_USER_STORE_STATE = "store-fb-user-state";
let LOCALSTORAGE_KEY_TINDER_AUTH_STORE_STATE = "store-tinder-auth-state";
let LOCALSTORAGE_KEY_TINDER_LOCATION_STORE_STATE = "store-tinder-location-state";
let LOCALSTORAGE_KEY_TINDER_MATCH_STORE_STATE = "store-tinder-match-state";
let LOCALSTORAGE_KEY_TINDER_RECOMMENDATION_STORE_STATE = "store-tinder-recommendation-state";
let LOCALSTORAGE_KEY_TINDER_SENTIMENT_STORE_STATE = "store-tinder-sentiment-state";
let LOCALSTORAGE_KEY_TINDER_UPDATE_STORE_STATE = "store-tinder-update-state";
let LOCALSTORAGE_KEY_TINDER_USER_STORE_STATE = "store-tinder-user-state";
let TINDER_CLIENT_ID = "464891386855067";

// URLs
let FB_AUTH_TOKEN_HREF = `https://www.facebook.com/dialog/oauth?client_id=${TINDER_CLIENT_ID}
&redirect_uri=https://www.facebook.com/connect/login_success.html
&auth_type=reauthenticate
&scope=basic_info,email,public_profile,user_about_me,user_activities,user_birthday,user_education_history,user_friends,user_interests,user_likes,user_location,user_photos,user_relationship_details
&response_type=token`;
let FB_CURRENT_USER_URL = "https://graph.facebook.com/me";
let TINDER_RECS_URL = "https://api.gotinder.com/user/recs";
let TINDER_LOCATION_URL = "https://api.gotinder.com/user/ping";
let TINDER_AUTH_URL = "https://api.gotinder.com/auth";
let TINDER_PROFILE_URL = "https://api.gotinder.com/profile";
let TINDER_UPDATES_URL = "https://api.gotinder.com/updates";

// URL template generators
function TINDER_SENTIMENT_URL(strings, ...values) {
  return `https://api.gotinder.com/${values[0]}/${values[1]}`;
}

function TINDER_MATCH_MESSAGE_URL(strings, ...values) {
  return `https://api.gotinder.com/user/matches/${values[0]}`;
}

function TINDER_USER_INFO_URL(strings, ...values) {
  return `https://api.gotinder.com/user/${values[0]}`;
}

function TINDER_DELETE_MATCH_URL(strings, ...values) {
  return `https://api.gotinder.com/user/matches/${values[0]}`;
}

// Tinder-related
let TINDER_HEADERS_WITHOUT_AUTH_TOKEN = {
  'Accept-Language': 'en;q=1, ru;q=0.9',
  'Accept-Encoding': 'gzip, deflate',
  'User-Agent': 'Tinder/4.1.4 (iPhone; iOS 8.0; Scale/2.00)',
  'os_version': '800000',
  'Accept': '*/*',
  'platform': 'ios',
  'Content-Type': 'application/json; charset=utf-8',
  'Connection': 'keep-alive',
  'Proxy-Connection': 'keep-alive',
  'app-version': '218'
};
let SENTIMENT_LIKE = "like";
let SENTIMENT_PASS = "pass";
let TINDER_MIN_FILTER_AGE = 18;
let TINDER_MAX_FILTER_AGE = 120;
let TINDER_GENDER_MALE = 0;
let TINDER_GENDER_FEMALE = 1;

let APP_BACKGROUND_CLASS =  "couple-on-bridge-bg";

export default {
  APP_BACKGROUND_CLASS,
  FB_AUTH_TOKEN_HREF,
  FB_CURRENT_USER_URL,
  LOCALSTORAGE_KEY_LOCALE_STORE_STATE,
  LOCALSTORAGE_KEY_ERROR_STORE_STATE,
  LOCALSTORAGE_KEY_FB_AUTH_STORE_STATE,
  LOCALSTORAGE_KEY_FB_USER_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_AUTH_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_LOCATION_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_MATCH_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_RECOMMENDATION_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_SENTIMENT_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_UPDATE_STORE_STATE,
  LOCALSTORAGE_KEY_TINDER_USER_STORE_STATE,
  SENTIMENT_LIKE,
  SENTIMENT_PASS,
  TINDER_AUTH_URL,
  TINDER_CLIENT_ID,
  TINDER_DELETE_MATCH_URL,
  TINDER_GENDER_FEMALE,
  TINDER_GENDER_MALE,
  TINDER_HEADERS_WITHOUT_AUTH_TOKEN,
  TINDER_LOCATION_URL,
  TINDER_MATCH_MESSAGE_URL,
  TINDER_MAX_FILTER_AGE,
  TINDER_MIN_FILTER_AGE,
  TINDER_PROFILE_URL,
  TINDER_RECS_URL,
  TINDER_SENTIMENT_URL,
  TINDER_UPDATES_URL,
  TINDER_USER_INFO_URL
};

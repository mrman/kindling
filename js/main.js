import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Link, IndexRoute } from "react-router";
import App from "./components/app";
import LoginPage from "./components/login-page";
import ProcessLoginPage from "./components/process-login-page";
import AppPage from "./components/app-page";
import RecsPage from "./components/recs-page";
import MatchesPage from "./components/matches-page";
import PreferencesPage from "./components/preferences-page";
import ErrorPage from "./components/error-page";

ReactDOM.render((
  <Router>
    <Route path="/" component={App}>
      <IndexRoute component={LoginPage}/>
      <Route name="process-login" path="/access_token*" component={ProcessLoginPage}/>
      <Route name="login" path="/login" component={LoginPage}/>
      <Route name="app" path="/app" component={AppPage}/>
      <Route name="recs" path="/recs" component={RecsPage}/>
      <Route name="matches" path="/matches" component={MatchesPage}/>
      <Route name="preferences" path="/preferences" component={PreferencesPage}/>
      <Route name="error" path="/error" component={ErrorPage}/>
    </Route>
  </Router>
),
  document.getElementById("app")
);

export default {
  app: {
    name: "Kindling",
    subtitle: "Un cliente de Tinder™ no oficial de FirefoxOS",
    tagline: {
      first: "Encontras un poquito",
      last: "calidez humana"
    }
  },

  general: {
    male: "masculino",
    female: "hembra",
    firstCapitalizedMale: "Masculino",
    firstCapitalizedFemale: "Hembra",
    firstCapitalizedLanguage: "Idioma"
  },

  nav: {
    loadingMessage: "Cargando...",
    back: "Volver",
    sections: {
      findAMatch: "Buscar una pareja",
      matches: "Parejas",
      preferences: "Preferencias",
      logout: "Cerrar Sesión"
    }
  },

  time: {
    xHours: "{} horas",
    xMinutes: "{} minutos",
    xSeconds: "{} segundos"
  },

  distances: {
    mile: {
      single: "milla",
      multiple: "millas"
    },
    milesAway: "millas de distancia",
    unitsAway: "{} {} de distancia"
  },

  pages: {

    preferences: {
      pageSubtitle: 'Cambias alquien tu puedes encontrar',
      genderSectionHeading: "Su sexo",
      ageSectionHeading: "Edad deseada",
      distanceSectionHeading: "Distancia deseada",
      locationSectionHeading: "Ubicación",
      fromGPS: "de GPS",
      customLocation: "Ubicación personalizado",
      updatePreferences: "Actualizar sus preferencias",
      undoChanges: "Deshacer sus cambios",
      inbetweenAges: "a"
    },

    login: {
      login: "Iniciar Sesión"
    },

    recs: {
      noRecsMessage: "No mas recomendaciones",
      getMoreRecs: "Obetener más recomendaciones",
      hitDailyLimitMaybe: "Podra ser que paso su limite diario de gusta, por favor trata mañana",
      updatePrefsMaybe: "No mas recomendaciones. Podría ser debido a sus preferencias.",
      managePrefs: "Manejar preferencias",
      friendsInCommon: "{} amigo en común",
      commonInterests: "{} interes en común",
      lastActive: "activo por última vez",
      keepSearching: "Continuar Buscando",
      like: "Gusta",
      recsFetchFailed: "No pudo obtener recomendaciones dos veces... Cierre y reinicio Kindling, por favor",
      outOfLikesHeading: "No mas gustas",
      outOfLikesMessage: "Podra ser que paso su limite diario de gusta",
      likesReplenishInXHours: "Cuando puedes gustar: "
    },

    matches: {
      pageSubtitle: "Habla con sus parejas",
      findSomeoneNew: "Buscar un pareja nuevo",
      noMatchesFound: "No mas parejas",
      lastMessageSent: "Último mensaje enviado",
      noMessagesSent: "No hay mensajes enviados"
    },
    matchConvo: {
      sending: "Enviando..."
    }
  }
};

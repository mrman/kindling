export default {
  app: {
    name: "Kindling",
    subtitle: "Firefoxの非公式Tinder™のclient",
    tagline: {
      first: "面白い人間関係",
      last: "一緒にさがそう"
    }
  },
  general: {
    male: "男の子",
    female: "女の子",
    firstCapitalizedMale: "男の子",
    firstCapitalizedFemale: "女の子",
    firstCapitalizedLanguage: "語"
  },
  nav: {
    loadingMessage: "ローディング…",
    back: "戻る",
    sections: {
      findAMatch: "探す",
      matches: "合わせた人",
      preferences: "セッチィンぐ",
      logout: "ログアウト"
    }
  },
  time: {
    xHours: "{}　時",
    xMinutes: "{} 分",
    xSeconds: "{} 秒"
  },
  distances: {
    mile: {
      single: "マイル",
      multiple: "マイル"
    },
    milesAway: "マイル",
    unitsAway: "{} {}"
  },
  pages: {
    preferences: {
      pageSubtitle: '探す設定を決めてください',
      genderSectionHeading: "性別",
      ageSectionHeading: "年齢",
      distanceSectionHeading: "最大距離",
      locationSectionHeading: "ロケーション",
      fromGPS: "GPSから",
      customLocation: "カスタムロケーション",
      updatePreferences: "設定をセーブする",
      undoChanges: "設定を戻す",
      inbetweenAges: "から"
    },
    login: {
      login: "ロぐイン"
    },
    recs: {
      noRecsMessage: "勧告はありません",
      getMoreRecs: "新しい勧告を探す",
      hitDailyLimitMaybe: "毎日のライクリミットを越えましたかもしりません。明日まで待って勧告を探してください。",
      updatePrefsMaybe: "探す設定のせいで勧告出来なかった可のせいもあります。",
      managePrefs: "探す設定を変わる",
      friendsInCommon: "{} 友人(共通）",
      commonInterests: "{} 趣味(共通）",
      lastActive: "最後に見た",
      keepSearching: "まだ勧告を見る",
      like: "ライク",
      recsFetchFailed: "勧告探しは二同出来ませんでしたのでKindlingをクロースして、もうオーペンしてください。",
      outOfLikesHeading: "ライクは無くなりました",
      outOfLikesMessage: "ライクは間もなく使えません。",
      likesReplenishInXHours: "ライクを使えるようになる時間:"
    },
    matches: {
      pageSubtitle: "合わせた人と話す",
      findSomeoneNew: "新しい人を探す",
      noMatchesFound: "よく合わせる人わ見つけられませんでした",
      lastMessageSent: "最後通信したのメッセージ",
      noMessagesSent: "メッセージはまだ送ってありません"
    },
    matchConvo: {
      sending: "送っています…"
    }
  }
};

export default {
  app: {
    name: "Kindling",
    subtitle: "Unofficial Tinder™ client for FirefoxOS",
    tagline: {
      first: "Find some",
      last: "human warmth"
    }
  },
  general: {
    male: "male",
    female: "female",
    firstCapitalizedMale: "Male",
    firstCapitalizedFemale: "Female",
    firstCapitalizedLanguage: "Language"
  },
  nav: {
    loadingMessage: "Loading...",
    back: "Back",
    sections: {
      findAMatch: "Find A Match",
      matches: "Matches",
      preferences: "Preferences",
      logout: "Logout"
    }
  },
  time: {
    xHours: "{} hours",
    xMinutes: "{} minutes",
    xSeconds: "{} seconds"
  },
  distances: {
    mile: {
      single: "mile",
      multiple: "miles"
    },
    milesAway: "miles away",
    unitsAway: "{} {} away"
  },
  pages: {
    preferences: {
      pageSubtitle: 'Manage who you can find',
      genderSectionHeading: "Your Sex",
      ageSectionHeading: "Desired Age",
      distanceSectionHeading: "Desired Distance",
      locationSectionHeading: "Location",
      fromGPS: "From GPS",
      customLocation: "Custom Location",
      updatePreferences: "Update preferences",
      undoChanges: "Undo changes",
      inbetweenAges: "to"
    },
    login: {
      login: "Login"
    },
    recs: {
      noRecsMessage: "No more recommendations",
      getMoreRecs: "Get More Recommendations",
      hitDailyLimitMaybe: "Looks like you might have hit your daily limit of likes! Please try again tomorrow.",
      updatePrefsMaybe: "Your preferences might also be keeping you from receiving recommendations.",
      managePrefs: "Manage Preferences",
      friendsInCommon: "{} friends in common",
      commonInterests: "{} common interests",
      lastActive: "last active",
      keepSearching: "Keep Searching",
      like: "Like",
      recsFetchFailed: "Fetching recommendations has failed more than once... Please close and restart Kindling",
      outOfLikesHeading: "Out of likes :(",
      outOfLikesMessage: "You've run out of likes on Tinder",
      likesReplenishInXHours: "Time until you may use more likes: "
    },
    matches: {
      pageSubtitle: "Talk with your matches",
      findSomeoneNew: "Find someone new",
      noMatchesFound: "No matches found",
      lastMessageSent: "Last message sent",
      noMessagesSent: "No messages sent"
    },
    matchConvo: {
      sending: "Sending..."
    }
  }
};

import Reflux from "reflux";

export default Reflux.createActions([
  "authenticateWithTinder",
  "changeLocaleByCode",
  "error",
  "getCurrentRec",
  "getNextTinderRecommendation",
  "like",
  "loggedIn",
  "login",
  "logout",
  "matchResultDetected",
  "outOfLikes",
  "pass",
  "pullTinderRecommendations",
  "receivedSentMessageReceipt",
  "republishTinderUser",
  "retrieveMatchedUserInfo",
  "retrieveTinderUpdates",
  "sendMessageToMatch",
  "sentimentSuccessfullyProcessed",
  "successfullyDeletedMatch",
  "updateLocationFromGPS",
  "updateTinderPreferences"
]);

import _ from "lodash";

/**
 * LocalStorageBackup mixin provides methods related to localstorage.
 * Initialization must be performed before the mixin can be used
 */
export default function(localStorageKey, stateGetterFn) {
  return {
    backupState() {
      // User state getter function (if provided) or this.state by default
      localStorage.setItem(localStorageKey, stateGetterFn ? stateGetterFn.bind(this)() : JSON.stringify(this.state));
    },

    restoreState() {
      let state = {};

      // Load localstorage state
      if (localStorageKey in localStorage) {
        let serializedStoredState = localStorage.getItem(localStorageKey);
        state = _.extend(state, JSON.parse(serializedStoredState));
      }

      return state;
    },

    gatherState() { return this.restoreState(); }

  };
}

import _ from "lodash";
import Constants from "./constants";
import Actions from "./actions";
import { getGlobalFetch, detectFFOSPlatform } from "./util";

let AUTHENTICATION_RETRIES = 0;

/**
 * Fetch from the tinder API
 *
 * @param {string} url - The URL to POST to
 * @param {string} method - Method to be used in the fetch request
 * @param {object} bodyJson - The body of the post request
 * @param {string} extraHeaders - Extra headers, if any
 * @returns {Promise} the fetch request promise
 */
function fetchFromTinderAPI(url, method="get", bodyJson={}, extraHeaders={}) {
  let combinedHeaders = _.extend(Constants.TINDER_HEADERS_WITHOUT_AUTH_TOKEN, extraHeaders);
  let opts = _.extend({}, {headers: combinedHeaders});

  // Add method if non-get method is specified
  if (method !== "get") {
    opts = _.extend(opts, {method: method});
    delete opts.body;
  }

  // Add body if specified if specified
  if (!_.isUndefined(bodyJson) &&
      !_.isNull(bodyJson) &&
      method === "post") {
    opts = _.extend(opts, {body: JSON.stringify(bodyJson)});
  }

  return getGlobalFetch()(url, opts);
}

export default {
  /**
   * Perform a POST request to the tinder API, with all requisite information
   *
   * @param {string} url - The URL to POST to
   * @param {object} bodyJson - The body of the post request
   * @returns {Promise} the fetch request promise
   */
  POST(url, bodyJson) {
    return fetchFromTinderAPI(url, "post", bodyJson);
  },

  /**
   * Perform a POST request to the tinder API, with access token provided
   *
   * @param {string} url - The URL to POST to
   * @param {object} bodyJson - The body of the post request
   * @param {string} token - The tinder API token
   * @returns {Promise} the fetch request promise
   */
  POSTWithToken(url, bodyJson, token) {
    return fetchFromTinderAPI(url, "post", bodyJson, {'X-Auth-Token': token})
      .then(this.detectAuthErrorResponse);
  },

  /**
   * Perform a GET request to the tinder API, with the access token provided
   *
   * @param {string} url - The URL to POST to
   * @param {string} token - The tinder API token
   * @returns {Promise} the fetch request promise
   */
  GETWithToken(url, token) {
    return fetchFromTinderAPI(url, "get", null, {'X-Auth-Token': token})
      .then(this.detectAuthErrorResponse);
  },

  /**
   * Attempt to reauthenticate on receiving a HTTP response with status 401 from the Tinder API.
   * When an authentication error does occur, this method will trigger the "authenticateWithTinder" action
   *
   * @param {object} resp - A fetch call response
   * @returns The response, or null in the case that an authentication did occur.
   */
  detectAuthErrorResponse(resp) {
    // Attempt to catch failures that need reauthentication
    if (_.get(resp, "status") == 401 && AUTHENTICATION_RETRIES < 5) {
      console.log("[TINDER API] Detected 401 response, possibly bad API token, reauthenticating...");
      AUTHENTICATION_RETRIES++;
      Actions.authenticateWithTinder();
      return null;
    }

    // Reset authentication retry counter
    AUTHENTICATION_RETRIES = 0;
    return resp;
  }

};

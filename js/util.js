import _ from "lodash";
import Constants from "./constants";
import URI from "uri.js";
import fetch from "vendor/fetch";

// Error patterns for Tinder's FB auth request redirect
const re_login_resp_err = /error=([^&]+)/;
const re_login_resp_err_info = {
    code: /error_code=([\d]+)&?/,
    type: re_login_resp_err,
    desc: /error_description=([^&]+)&?/,
    message: /error_reason=([^&]+)&?/
};

// Success patterns for Tinder's FB auth request redirect
const re_login_resp_success = /access_token=([^&]+)/;
const re_login_resp_success_info = {
    accessToken: /access_token=([^&]+)&/,
    expiresIn: /expires_in=([\d]+)/
};

/**
 * Generate a URL with the appropriate query parameters attached
 *
 * @param {string} base - The base URL (ex. "https://somesite.com/api/")
 * @param {object} params - An object that will be search for parameters
 * @returns a URL object
 */
export function generateURLWithQueryParams(base, params) {
  let url = new URI(base);
  _.each(params, (val, key) => url.addQuery(key, val));
  return url.toString();
}

/**
 * Perform a POST request with a JSON body
 *
 * @param {object} url - The URL to POST to
 * @param {object} json - An object that will be used as the body of the request
 * @return {Promise} The fetch request promise
 */
export function jsonPOSTRequest(url, json) {
  return getGlobalFetch()(
    url,
    {
      method: "post",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(json)
    });
}

/**
 * Parse a given location.hash object for a successful/failed login
 *
 * @param {string} hash - The output of location.hash
 * @returns {accessToken: <string>, expiresIn: <string>} | {code: <string>, type: <string>, desc: <string>, message <string>}
 */
export function parseLocationHashForLoginResp(hash) {
  // Check if an error occurred, determine which map to build info object with
  let [errorOccurred, successOccurred] = [re_login_resp_err.exec(hash), re_login_resp_success.exec(hash)];

  // Quit early if hash matches neither expected situation
  if (!errorOccurred && !successOccurred) { return null; }

  // Build info object to return from the known regexes
  let info_patterns = _.isEmpty(successOccurred) ? re_login_resp_err_info : re_login_resp_success_info;
  return _.reduce(info_patterns, (acc, v, k) => {
    // Read the key from the info hash, execute the regex that is the value
    let match = v.exec(hash);
    if (!_.isEmpty(match)) { acc[k] = match[1]; }
    return acc;
  }, {});
}

/**
 * Helper for toggling a variable on state in a react Component
 *
 * @param {string} prop - The state property to toggle
 * @param {Object} comp - The react component (usually passed in as "this")
 */
export function toggleStateBoolean(prop, comp) {
  return () => {
    let statePartial = {};
    statePartial[prop] = !comp.state[prop];
    comp.setState(statePartial);
  };
}

export function isValidTinderUserPreferencesObject(prefs) {
  return _.has(prefs, "distance_filter") &&
    !_.isNaN(prefs.distance_filter) &&
    _.isNumber(prefs.distance_filter) &&
    _.has(prefs, "age_filter_min") &&
    !_.isNaN(prefs.age_filter_min) &&
    _.isNumber(prefs.age_filter_min) &&
    _.has(prefs, "age_filter_max") &&
    !_.isNaN(prefs.age_filter_max) &&
    _.isNumber(prefs.age_filter_max) &&
    _.has(prefs, "gender");
}

/**
 * Helper function for a text area based on the value from a change event (ex textareas, input boxes)
 *
 * @param {string} propName - The name of the property to be updated
 */
export function updatePropertyWithEventTargetValue(propName, comp) {
  return (e) => {
    let statePartial = {};
    statePartial[propName] = e.target.value;
    comp.setState(statePartial);
  };
}

/**
 * Detect if app is on Firefox OS (assumed browser otherwise)
 */
export function detectFFOSPlatform() {
  return !_.isNull(window.navigator.userAgent.match(/Mozilla.*Mobile;/));
}

/**
* Unfortunately fetch seems to not work properly with ES6 imports, in both E2E test and unit test environment
* hacked around this by just checking when it was pulled in by window ('fetch' global) and when the SystemJS did it ('fetch.fetch')
*/
export function getGlobalFetch() {
  return (_.isFunction(fetch) ? fetch : fetch.fetch);
}

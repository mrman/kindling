# Kindling

Kindling is an unofficial [Tinder](http://www.gotinder.com/) client for FirefoxOS.

## Prerequisites

- [NodeJS](http://nodejs.org)
- [NPM](http://npmjs.org)
- [JSPM](http://jspm.io)

## Getting started

0. `npm install -g jspm`
1. `npm install`
2. `npm install -g jspm-bower-endpoint`
3. `jspm registry create bower jspm-bower-endpoint`
4. `jspm install`
5. `make`

## Getting started (development)

To setup development environment, run:

`make setup-dev`

(this will do various things like setting up the pre-commit hook)

To run the development build (which refreshes on file changes, using [entr](http://www.entrproject.org), run:

`make watch`

(this runs entr, which utilizes jspm to rebuild `dist/` when important files change)

## Running tests

### Unit tests
To run the unit tests for Kindling, run:

`make test`

### End to end tests
To run the end to end tests for Kindling, run:

`make test-e2e`

#### Running an individual E2E test (with mocha)

Running individual tests inside the suite can be done with mocha:

`mocha --compilers js:babel-register test/e2e/some-page.js`

If you'd like to see debug messages from nightmare, make sure to include `DEBUG=nightmare` like so:

`DEBUG=nightmare mocha --compilers js:babel-register test/e2e/some-page.js`

## Contributing

0. Fork this repo
1. Add fixes
2. Run the unit and end to end tests
3. Make a PR
